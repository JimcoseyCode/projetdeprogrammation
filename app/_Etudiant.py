
    
import json


class Answer_Question_Diffusion:
    def __init__(self) -> None:
        pass


class Etudiant:
    # ! Objet qui stocke tout les infos d'un étudiants
    nom = None,  # ? type(String)
    prenom =  None,  # ? type(String)
    numero_etudiant = None,  # ? type(Integer)
    mot_de_passe = None,
    info_students = {
        "nom": None,  # ? type(String)
        "prenom": None,  # ? type(String)
        "numero_etudiant": None,  # ? type(Integer)
        "mot_de_passe": None,
    }

    def __init__(self, nom_etudiant, prenom_etudiant, num_etudiant):
        # [] => remplissage des info de l'etudiant
        self.info_students["nom"] = nom_etudiant
        self.info_students["prenom"] = prenom_etudiant
        self.info_students["numero_etudiant"] = num_etudiant
        self.info_students["mot_de_passe"] = num_etudiant
        
        self.nom = nom_etudiant
        self.prenom = prenom_etudiant
        self.numero_etudiant = num_etudiant
        self.mot_de_passe = num_etudiant
        

    def modifier_Profile(self,num_etudiant,age,nationalite,parcours,sexe,classe,mdp,ancien_mdp):
        with open("./components/user_info/students_DataBase.json","r") as file:
            data = json.load(file)

            for student in data:
                if data[student]["numero_etudiant"] == num_etudiant:
                    data[student]["age"] = age
                    data[student]["nationalite"] = nationalite
                    data[student]["parcours_scolaire"] = parcours
                    data[student]["sexe"] = sexe
                    data[student]["classe"] = classe
                    if data[student]["mot_de_passe"] == ancien_mdp:
                        data[student]["mot_de_passe"] = mdp
 
            with open("./components/user_info/students_DataBase.json", "w") as file:
                json.dump(data,file,indent=4)


    def get_Profile(self,num_etudiant):
        objectInfo = {
            "nom": "" ,
            "prenom": "",
            "numero_etudiant": "",
            "mot_de_passe": "",
            "age": "",
            "nationalite": "",
            "parcours_scolaire": "",
            "sexe": "",
            "classe": "",
        }

        with open("components/user_info/students_DataBase.json","r") as file:
            data = json.load(file)
        for student in data:
            if data[student]["numero_etudiant"] == num_etudiant:
                objectInfo["nom"] = data[student]["nom"] 
                objectInfo["prenom"] = data[student]["prenom"]
                objectInfo["numero_etudiant"] = data[student]["numero_etudiant"]
                objectInfo["mot_de_passe"] = data[student]["mot_de_passe"]
                objectInfo["age"] = data[student]["age"]
                objectInfo["nationalite"] = data[student]["nationalite"]
                objectInfo["parcours_scolaire"] = data[student]["parcours_scolaire"]
                objectInfo["sexe"] = data[student]["sexe"]
                objectInfo["classe"] = data[student]["classe"]
        
        return objectInfo
    