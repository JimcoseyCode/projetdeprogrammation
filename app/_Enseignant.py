import pandas as pd
import json
import glob
import random
import requests
import shutil
from Enseignant_GestionQuestion import GestionQuestion
from __Module_utility_Function__ import student_exist,add_object_to_json_student
from __LiveStream__ import QuestionLive,SequenceLive

# ! Hérite de l'objet gestion question

class Enseignant(GestionQuestion):
    user_name = ""
    user_password = ""
    question_stock = []
    file_questionLink = None
    # Stocke les instances des lives encours de l'enseignant
    _current_LIVE_STREAM = None

    def __init__(self, u_name, u_pswd):
        self.user_name = u_name
        self.user_password = u_pswd
        self.file_questionLink = (
            f"components/user_info/Question_dataBase/{self.user_name}_question.json"
        )
        super().__init__(u_name)
    

    def create_student_account(self, path_file_dataBase_students):
        data_DataBase_Student = pd.read_csv(path_file_dataBase_students) #object
        for index_line, student_line in data_DataBase_Student.iterrows():
            print(student_line["numero_etudiant"])
            # L'étudiant n'existe pas alors on l'ajoute dans la base de donnée
            if student_exist(str(student_line["numero_etudiant"])) == False: #definit par la premier lignee pour chaque colone sont nom 
                student_object = {
                    "nom": student_line["nom"],
                    "prenom": student_line["prenom"],
                    "numero_etudiant": str(student_line["numero_etudiant"]),
                    "mot_de_passe": str(
                        student_line["numero_etudiant"]
                    ),  # ! mot passe par default : -> (NUM_ETUDIANT)
                    "age":"",
                    "nationalite":"",
                    "parcours_scolaire":"",
                    "sexe":"",
                    "classe":""
                }
                add_object_to_json_student(
                    student_object,
                    "student",
                    "components/user_info/students_DataBase.json",
                )
                # Génération de photo de profile pour chaque utilisateur crée
                category=["nature","city","technology","food","still_life","abstract","wildlife"]
                api_url = 'https://api.api-ninjas.com/v1/randomimage?category={}'.format(category[random.randint(0,len(category)-1)])
                response = requests.get(api_url, headers={'X-Api-Key': 'ZNJfEV9LnSTx5w/jzN8hFA==QyAZzoh8mh1707AL', 'Accept': 'image/jpg'}, stream=True)
                if response.status_code == requests.codes.ok:
                    with open(f"./static/profile_user/img/{student_object['numero_etudiant']}.jpg", 'wb') as out_file:
                        shutil.copyfileobj(response.raw, out_file)
                else:
                    print("Error:", response.status_code, response.text)

        return "L'ajout des étudiants a bien été efféctuée"
    # Give info Data enseignant en fonction des flags
    def getInfo_user(self, user_name, type_donne):
        
        if type_donne == "all_questions":
            all_questions = []
            with open(
                f"components/user_info/Question_dataBase/{user_name}_question.json", "r"
            ) as file_:
                questions = json.load(file_)
                for q_ in questions:
                    all_questions.append(questions[q_])
            return all_questions 
        
        elif type_donne == "all_pages":
            object_User_formated = {"user_name": user_name, "all_pages": {}}
            array_path_all_page = glob.glob(
                f"components/user_info/Page_dataBase/{user_name}*.json"
            )
            for page in array_path_all_page:
                with open(page, "r") as file_:
                    buffer_read = json.load(file_)
                    for key in buffer_read:
                        object_User_formated["all_page"].append(buffer_read[key])
            return object_User_formated

        elif type_donne == "all_etiquettes":
            object_User_formated = {"all_etiquettes": []}
            with open(
                f"components/user_info/Etiquete/{user_name}_etiquette.json", "r"
            ) as file_:
                buffer_read = json.load(file_)
                for key in buffer_read:
                    object_User_formated["all_etiquettes"].append(
                        buffer_read[key]["thematique"]
                    )
            return object_User_formated["all_etiquettes"]
    def _change_password(self,older_password,new_password):
        with open("./components/user_info/all_user_password.json", "r") as dataBase:
            buffer_read = json.load(dataBase)
            # print(buffer_read)
            for enseignant in buffer_read.values():
                if (enseignant["user_name"] == self.user_name and enseignant["user_password"] == older_password):
                    enseignant["user_password"] = new_password
                    with open("./components/user_info/all_user_password.json", "w") as file_:
                            json.dump(buffer_read, file_, indent=4)
                    return True
            return False
            



    def _create_live_stream(self,type_of_live,o_id_questionSelected):

        if (type_of_live == "QLive" and len(o_id_questionSelected) == 1):
            o_List_Questions = (self.getSpecifiqueQuestion(o_id_questionSelected[0]))
            print(o_List_Questions) #afficher les attributs
            # Ajout de l'instance du live contenant la questions
            self._current_LIVE_STREAM = None
            self._current_LIVE_STREAM = (QuestionLive(o_List_Questions,self.user_name))
            self._current_LIVE_STREAM.current_student_participant = []
            # self._current_LIVE_STREAM.data_answer_student = {} 
            # self._current_LIVE_STREAM.stat_answer = {}

            return f"{self._current_LIVE_STREAM.id_question_live} est en cours !"
        elif (type_of_live == "SeqLive" and len(o_id_questionSelected) >= 2):
            o_List_Questions = []
            for id in o_id_questionSelected:
                o_List_Questions.append(self.getSpecifiqueQuestion(id))
            print("Nombre de questions ajouté : "+str(len(o_List_Questions)))
            self._current_LIVE_STREAM = None
            self._current_LIVE_STREAM = SequenceLive(o_List_Questions,self.user_name)
            # self._current_LIVE_STREAM.current_student_participant = []
            # self._current_LIVE_STREAM.data_answer_student = {} 
            # self._current_LIVE_STREAM.stat_answer = {}

            return f"{self._current_LIVE_STREAM.id_sequence_live} est en cours !"
        
    def _stop_live_stream(self,type_live):
        if (type_live == "QLive"):
            self._current_LIVE_STREAM = None
        elif (type_live == "SeqLive"):
                    self._current_LIVE_STREAM = None
        else:
            return "Impossible de stopper le live"
        
    # donne un objet qui dit combien de question disponible par theme 
    def _get_all_Thematique(self):
        with open(
            f"./components/user_info/Etiquete/{self.user_name}_etiquette.json"
        ) as file_:
            # ? Stock file(type(JSON)) => dictionaire
            stock_dic_JSON = json.load(file_)
            array_etiquette_server = []
            for key in stock_dic_JSON:
                array_etiquette_server.append(stock_dic_JSON[key]["thematique"])
            # print(array_etiquette_server)
            return array_etiquette_server
    def _get_thematique_stat(self):
        obj_ = {

        }
        for thematique in self._get_all_Thematique():
            # print(self.getQuestion_by_theme([thematique]))
            # print()
            obj_[thematique] = len(self.getQuestion_by_theme([thematique]))
        # print(obj_)
        return obj_