from random import randint
import secrets
import os
import json
import datetime
# from flask_socketio import emit, SocketIO
# from app import socketio
"""
    ? Gerer les diffusion de question && diffusion de sequence de question
        - Class( QuestionLive , SequenceLive)
        - QuestionLive :: -> Un id d'une question est passée en parametre 
            * chercher dans la base de donnée la question l'instancie et les etudiants pourront utiliser le saisie de reponse de cette question
"""
class QuestionLive:
    id_uLive = None
    id_question_live = None  # genere un id pour la question
    question_live_content = []  # stocke une instance de type [Question]
    current_student_participant = []
    link_dataBase_Question_live = None
    __bool_answer_question = True
    _nbr_answered = 0
    # ! Enregistre les stats des etudiants qui ont repondu a chaque proposition
    stat_answer = {}
    owner_live = None  # proprietaire
    type_live = None
    # Trace de qui a repondu a la question courante pour qu'il ne puisse plus repondre
    data_answer_student = {}


    def _format_data(self):
        self.id_uLive = None
        self.id_question_live = None  
        self.question_live_content = [] 
        self.current_student_participant = []
        self.link_dataBase_Question_live = None
        self.__bool_answer_question = True
        self._nbr_answered = 0
        self.stat_answer = {}
        self.owner_live = None 
        self.type_live = None
    # ? :: -> DATABASE (nbr_question repondu par etudiant)

    def __init__(self, q_, user_name_enseignant):
        # Format data
        self._format_data()
        # 
        self.id_question_live = "qLive - " + secrets.token_hex(4)
        self.id_uLive = self.id_question_live
        self.owner_live = user_name_enseignant
        self.question_live_content = q_
        # print(self.question_live_content)
        self.type_live = "QLive"
        # print(self.id_uLive)
        # ? remplissage des données statistiques de la question
        # ? Dictionnaire contenant en clé la proposition et en valeur le nombre de gens ayant repondu cette question
        # print(self.question_live_content[0])
        for propositions in self.question_live_content.Q_proposition:
            self.stat_answer[propositions] = 0
        # Stockage des données du live
        object_live_info = {
            "owner_live": self.owner_live,
            "id_live": self.id_uLive,
            "live_type": "QLive",
            "date_live_stream": str(datetime.datetime.now()),
            # Les données des etudiants dans le live sont stocké
            "espace_students": {
                # data students
            }
        }
        
        if (os.path.exists(f"./components/LiveStream_database/{user_name_enseignant}_Data_LIVE")):
            with open(
                f"./components/LiveStream_database/{user_name_enseignant}_Data_LIVE/{self.id_question_live}_Data.json", "w"
            ) as file_:
                json.dump(object_live_info, file_, indent=4)
        self.link_dataBase_Question_live = f"components/LiveStream_database/{user_name_enseignant}_Data_LIVE/{self.id_question_live}_Data.json"

    # ! Methods
    # Lie l'etudiant au live
    def _connect(self, etudiant_instance):
        if etudiant_instance not in self.current_student_participant:
            self.current_student_participant.append(etudiant_instance)
            # ? Ajouter l'etudiant dans le fichier JSON du live
            _object_Student_Live = {
                "info_student": {
                    "numero_etudiant": etudiant_instance.numero_etudiant,
                    "nom":  etudiant_instance.nom,
                    "prenom": etudiant_instance.prenom,
                },
                # Stocke les reponses soumis par l'etudiant
                "answer_live": {

                }
            }

            self.data_answer_student[str(
                _object_Student_Live["info_student"]["numero_etudiant"])] = True
            # Ajout des des données dans le fichier
            with open(self.link_dataBase_Question_live, "r") as file_:
                data = json.load(file_)
            key = f"student_{etudiant_instance.numero_etudiant}"
            data["espace_students"][key] = _object_Student_Live
            # écrire les données modifiées dans le fichier JSON
            with open(self.link_dataBase_Question_live, "w") as file_:
                json.dump(data, file_, indent=4)



     
            

    def _quit_Live(self, num_etudiant):
        print("je susi entrain de quittez")
        print(self.current_student_participant)
        for student in self.current_student_participant:
            if student.numero_etudiant == num_etudiant:
                # suprime l'etudiant pour se deconnecté
                self.current_student_participant.remove(student)
                # ? Met a jour le nombres d'etudiant connectées
                print(f"{num_etudiant} vous avez été deconnectée !")
        print(f"{num_etudiant} ça n'a pas fonctionée ")

        return f"{num_etudiant} vous avez été deconnectée !"
    # Renvoie le nombre de question repondu d'un etudiant

    def _nbr_question_answered_by_student(self, numero_etudiant):
        # ? specifications
        """
        f :: (num_etudiant) => nbr reponses repondu avant 

        """
        with open(self.link_dataBase_Question_live, "r") as file_:
            data = json.load(file_)
            return len(data["espace_students"][f"student_{numero_etudiant}"]["answer_live"])

    def _answer_question(self, num_etudiant, answer_saisie):
        if (self.__bool_answer_question == True and self.data_answer_student[num_etudiant] == True):
            # ? Enregistrement des données de reponses de chaque etudiant qui repond a une question
            object_reponse_student = {
                "id_question_answered": self.question_live_content.Id_question,
                "date": str(datetime.datetime.now()),
                "answer": self.question_live_content.response_is_correct(answer_saisie)
        
            }
            obj_reponse_QO = {
                "id_question_answered": self.question_live_content.Id_question,
                "date": str(datetime.datetime.now()),
                # enregistrement de la reposne
                "answer": answer_saisie,
            }
            # self.data_answer_student[num_etudiant]
            # print(self.data_answer_student)
            data_h = None
            with open(self.link_dataBase_Question_live, "r") as file_:
                data = json.load(file_)
                print(data)
                # key_question = "answer" + str(self._nbr_question_answered_by_student(num_etudiant)+1)
                key_question = "answer" + \
                    str(len(data["espace_students"]
                        [f"student_{num_etudiant}"]["answer_live"])+1)
                print("Avant qo")
                if (self.question_live_content.type_question == "QO"):
                    print("Je susi rentrée")
                    data["espace_students"][f"student_{num_etudiant}"]["answer_live"][key_question] = obj_reponse_QO

                else:
                    print("ici else")
                    data["espace_students"][f"student_{num_etudiant}"]["answer_live"][key_question] = object_reponse_student
                    
                data_h = data
            with open(self.link_dataBase_Question_live, "w") as file_:
                json.dump(data_h, file_, indent=4)
            self.data_answer_student[num_etudiant] = False

            print(f" -- > Student answer tarce : {self.data_answer_student}")

            #  ** STATITISQUE UPDATE ** #
            self._nbr_answered += 1
            if self.question_live_content.type_question == "QN":
                pass
            else:
                for answer in answer_saisie:
                    if (answer in self.stat_answer):
                        self.stat_answer[answer] += 1

        else:
            print(f"Vous ne pouvez plus repondre : {self.owner_live}")
            return f"Vous ne pouvez plus repondre : {self.owner_live}"

    def _getStat_answer(self):
        print(f"Stat du live : {self.id_uLive} -> {self.stat_answer}")
        return self.stat_answer

    def _get_answer(self):
        return self.question_live_content.Q_responses

    def _stop_answer(self):
        self.__bool_answer_question = False
        print("Aucune reponses peut etre saisie")

    def number_participant(self):
        print(f":: -> Instance Live ID : {self.id_uLive} -> Nbr_connected {len(self.current_student_participant)}")
        if (len(self.current_student_participant) == 1):
            print(f"voici le premier etudiant de ce live : {self.current_student_participant[0].info_students}")
        print(self.current_student_participant)
        return len(self.current_student_participant)
    # envoie le nombre de personne qui ont repondu a la question

    def number_answered(self):
        return self._nbr_answered


class SequenceLive:
    id_uLive = None
    id_sequence_live = None  # genere un id pour les questions
    current_student_participant = []
    link_dataBase_Seq_live = ""
    # ! -- ! #
    _current_questionLive = []  # Pile contenant la question encours
    # stocke une liste d'instance de type [Question]
    question_live_content_ordered = list()
    # ! -- ! #
    __bool_answer_question = True
    _nbr_answered = 0
    # ! Enregistre les stats des etudiants qui ont repondu a chaque proposition
    stat_answer = {}
    owner_live = None  # proprietaire
    type_live = None
    # Trace de qui a repondu a la question courante pour qu'il ne puisse plus repondre
    data_answer_student = {}

    def _format_data(self):
        self.id_uLive = None
        self.id_sequence_live = None  
        self.current_student_participant = []
        self.link_dataBase_Seq_live = ""
        self._current_questionLive = []  
        self.question_live_content_ordered = list()
        self.__bool_answer_question = True
        self._nbr_answered = 0
        self.stat_answer = {}
        self.owner_live = None  
        self.type_live = None
       
        self.data_answer_student = {}

    def __init__(self, q_stock_ordered, user_name_enseignant):
        self._format_data()
        self.id_sequence_live = "SeqLive - " + secrets.token_hex(4)
        self.id_uLive = self.id_sequence_live  # id unique
        self.owner_live = user_name_enseignant
        self.question_live_content_ordered = q_stock_ordered
        self.type_live = "SeqLive"

        # ? Demarage de la premiere question dans la pile
        self._current_questionLive = self.question_live_content_ordered.pop(0)
        """
            ? DATA QUESTION ENCOURS
        """
        # ? remplissage des données statistiques de la question [encours]
        # ? Dictionnaire contenant en clé la proposition et en valeur le nombre de gens ayant repondu cette question
        for propositions in self._current_questionLive.Q_proposition:
            self.stat_answer[propositions] = 0
        object_live_info = {
            "owner_live": self.owner_live,
            "id_live": self.id_uLive,
            "live_type": "SeqLive",
            "date_live_stream": str(datetime.datetime.now()),
            # Les données des etudiants dans le live sont stocké
            "espace_students": {
                # data students
            }
        }
        # Stockage des données du live
        if (os.path.exists(f"./components/LiveStream_database/{user_name_enseignant}_Data_LIVE")):
            with open(
                f"./components/LiveStream_database/{user_name_enseignant}_Data_LIVE/{self.id_sequence_live}_Data.json", "w"
            ) as file_:
                json.dump(object_live_info, file_, indent=4)
        self.link_dataBase_Seq_live = f"./components/LiveStream_database/{user_name_enseignant}_Data_LIVE/{self.id_sequence_live}_Data.json"

    # a enlever le s

    # ! Methods
    # Lie l'etudiant au live

    def _connect(self, etudiant_instance):
        if etudiant_instance not in self.current_student_participant:
            self.current_student_participant.append(etudiant_instance)
            _object_Student_Live = {
                "info_student": {
                    "numero_etudiant": etudiant_instance.numero_etudiant,
                    "nom":  etudiant_instance.nom,
                    "prenom": etudiant_instance.prenom,
                },
                # Stocke les reponses soumis par l'etudiant
                "answer_live": {

                }
            }
            self.data_answer_student[str(
                _object_Student_Live["info_student"]["numero_etudiant"])] = True

            # Ajout des des données dans le fichier
            with open(self.link_dataBase_Seq_live, "r") as file_:
                data = json.load(file_)
            key = f"student_{etudiant_instance.numero_etudiant}"
            data["espace_students"][key] = _object_Student_Live
            # écrire les données modifiées dans le fichier JSON
            with open(self.link_dataBase_Seq_live, "w") as file_:
                json.dump(data, file_, indent=4)

    def _quit_Live(self, num_etudiant):
        for student in self.current_student_participant:
            if student.numero_etudiant == num_etudiant:
                # suprime l'etudiant pour se deconnecté
                self.current_student_participant.remove(student)
        return f"{num_etudiant} vous avez été deconnectée !"
    def _is_include(self,array_):
        if (array_[1] in self._current_questionLive.Q_proposition):
                return True
        return False
    def _answer_question(self, num_etudiant, answer_saisie):

        if (self.__bool_answer_question == True and self.data_answer_student[num_etudiant] == True):
            # ? Enregistrement des données de reponses de chaque etudiant qui repond a une question
            object_reponse_student = {
                "id_question_answered": self._current_questionLive.Id_question,
                "date": str(datetime.datetime.now()),
                # enregistrement de la reposne
                "answer": self._current_questionLive.response_is_correct(answer_saisie),
            }
            obj_reponse_QO = {
                "id_question_answered": self._current_questionLive.Id_question,
                "date": str(datetime.datetime.now()),
                # enregistrement de la reposne
                "answer": answer_saisie,
            }

            data_h = None
            with open(self.link_dataBase_Seq_live, "r") as file_:
                data = json.load(file_)
                # key_question = "answer" + str(self._nbr_question_answered_by_student(num_etudiant)+1)
                key_question = "answer" + \
                    str(len(data["espace_students"]
                        [f"student_{num_etudiant}"]["answer_live"])+1)
                if (self._current_questionLive.type_question == "QO"):
                    data["espace_students"][f"student_{num_etudiant}"]["answer_live"][key_question] = obj_reponse_QO
                else:
                    data["espace_students"][f"student_{num_etudiant}"]["answer_live"][key_question] = object_reponse_student
                data_h = data
            with open(self.link_dataBase_Seq_live, "w") as file_:
                json.dump(data_h, file_, indent=4)
                

            self.data_answer_student[num_etudiant] = False
          
            print(f" -- > Student answer trace : {self.data_answer_student}")
            #  ** STATITISQUE UPDATE ** #
            self._nbr_answered += 1
            print("Jer suis entrain de repondre a une question seqLive courant")
            print(f"Tableau de stat answer {self.stat_answer}")
            if self._current_questionLive.type_question == "QN":
                pass
            else:
                for answer in answer_saisie:
                    if (answer in self.stat_answer):
                        self.stat_answer[answer] += 1

        else:
            print(f"Vous ne pouvez plus repondre : {self.owner_live}")
            return f"Vous ne pouvez plus repondre : {self.owner_live}"

    def _nbr_question_answered_by_student(self, numero_etudiant):
        with open(self.link_dataBase_Seq_live, "r") as file_:
            data = json.load(file_)
            return len(data["espace_students"][f"student_{numero_etudiant}"]["answer_live"])

    def _nextQuestion(self):
        self._reset_data_current_Question()
        # met dans la pile la question suivante
        if self.question_live_content_ordered != []:
            self._current_questionLive = self.question_live_content_ordered.pop(
                0)
            for propositions in self._current_questionLive.Q_proposition:
                self.stat_answer[propositions] = 0
        for student in self.data_answer_student:
            self.data_answer_student[student] = True
        print(f" -> Le data (etudiant et avoir repondu ) {self.data_answer_student}")

    def _reset_data_current_Question(self):
        # vide la pile
        self._current_questionLive = None
        # capacité de repondre une question remise par default
        self.__bool_answer_question = True
        # le nombre de reponse repondu remis a zero
        self._nbr_answered = 0
        # renitialisation des stats
        self.stat_answer = {}

    def _getStat_answer(self):
        return self.stat_answer

    def _get_answer(self):
        return self._current_questionLive.Q_responses

    def _stop_answer(self):
        self.__bool_answer_question = False

    def number_participant(self):
        return len(self.current_student_participant)
    # envoie le nombre de personne qui ont repondu a la question

    def number_answered(self):
        return self._nbr_answered
