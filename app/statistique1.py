import array
import json
from Enseignant_Question import Question
from _Enseignant import Enseignant
import requests
import os
from __LiveStream__ import QuestionLive
from _Etudiant import Etudiant
from datetime import datetime
from flask_socketio import SocketIO, send, emit, join_room
from flask import Flask, render_template, request, redirect


class Statistique:

    user_name = None
    link_database_livestream = None

    def __init__(self, user_name):
        self.user_name = user_name
        self.link_database_livestream = (
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE"
        )

    def _give_students_Info(self):
        # ? specification ->
        """
        donner les infos de chaque etudiant ayant participer dans des lives d'un enseignant 
        """
        object_final = {}  # dict -> qui contient info students
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(self.link_database_livestream):
                with open(f"{self.link_database_livestream}/{file_name}") as file_:
                    data_file = json.load(file_)["espace_students"]
                    if (data_file != {}):
                        for student in data_file.values():
                            object_students = {
                                "info_student": {
                                    "numero_etudiant": student['info_student']['numero_etudiant'],
                                    "nom": student['info_student']['nom'],
                                    "prenom": student['info_student']['prenom'],
                                }
                            }
                            object_final[f"student_{student['info_student']['numero_etudiant']}"] = (
                                object_students)
        return object_final

    def _give_stat_Student(self, numero_etudiant):  # students_xxxx
        # ? specifications 
        """
        donne les stat de chaque etudiant -> un object contenant 2 tableau
            l'un qui contient les info de type qlive
          et l'autre seqlive
        """
        p_live_content = {
            "q_live":[],
            "seq_live":[]
        } # Object contenenant tout les les infos des lives que l'etudiant a participé
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(self.link_database_livestream):
                with open(f"{self.link_database_livestream}/{file_name}") as file_:
                    data_file = json.load(file_)
                    if (data_file["live_type"] == "QLive" and f"student_{numero_etudiant}" in data_file["espace_students"]):
                        if (data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"] != {}):
                            for answer in data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"].values():
                                object_answer = {
                                    "id_live": data_file["id_live"],
                                    "id_question": answer["id_question_answered"] ,
                                    "answer": answer["answer"], 
                                }
                                p_live_content["q_live"].append(object_answer)

                    if (data_file["live_type"] == "SeqLive" and f"student_{numero_etudiant}" in data_file["espace_students"]):
                        if (data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"] != {}):
                            for answer in data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"].values():
                                object_answer = {
                                    "id_live": data_file["id_live"],
                                    "id_question": answer["id_question_answered"] ,
                                    "answer": self.response_correcte_pourcentage_seqlive(data_file["id_live"],numero_etudiant), 
                                }
                                p_live_content["seq_live"].append(object_answer)
            return p_live_content
                              
            

        #                         object_final[f"student_{student['info_student']['numero_etudiant']}"] = (
        #                             object_students)
        # return object_final

    # attention pour les fichiers json ca itere sur les cles pas sur les objects

    # donne le nombre de reponse correcte d'un etudiant d'un qlive/SeqLive

    def response_good_by_students(self, id_live, num_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            s = 0
            for student in data["espace_students"].values():
                if student["info_student"]["numero_etudiant"] == num_etudiant:
                    # print(student["info_student"])
                    for cle_answer in student["answer_live"].values():
                        if cle_answer["answer"] == True:
                            s += 1
            return s

    # donne les reponses de chaque live(qlive/seqlive) d'un etudiant
    def response_good1(self, num_etudiant):
        student_object = {
            f"student_{num_etudiant}": {
                "info_student": {
                    "numero_etudiant": num_etudiant,
                    "nom": None,
                    "prenom": None,
                    "reponse_correct": None,
                },
                "Live": {},
            },
        }
        # l'ensemble des question
        # question_ = {}
        print(os.path.exists(self.link_database_livestream))
        if os.path.exists(self.link_database_livestream) == True:
            for file_name in os.listdir(self.link_database_livestream):
                print(file_name)
        #         with open(f"{self.link_database_livestream}/{file_name}") as file:
        #             data = json.load(file)
        #             # student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"] = {
        #             # }
        #             student_object[f"student_{num_etudiant}"]["fofo"][f"live_{data['id_live'][-8:len(data['id_live'])]}"] = {
        #             }
        #             print("hey")
        # print(student_object)
                #     if data['live_type'] == "QLive":
                #         student[f"student_{num_etudiant}"]["info_student"]["reponse_correct"] = self.response_good_by_students(
                #             data['id_live'], num_etudiant)
                #     elif data['live_type'] == "SeqLive":
                #         student[f"student_{num_etudiant}"]["info_student"]["reponse_correct"] = self.response_correcte_pourcentage_seqlive(
                #             data['id_live'], num_etudiant)
                #     for etudiant in data["espace_students"].values():
                #         # print(etudiant)
                #         if etudiant["info_student"]["numero_etudiant"] == num_etudiant:
                #             student[f"student_{num_etudiant}"]["info_student"]["nom"] = etudiant["info_student"]["nom"]
                #             student[f"student_{num_etudiant}"]["info_student"]["prenom"] = etudiant["info_student"]["prenom"]
                #             # print(student[f"student_{num_etudiant}"])
                #             for cle_answer in etudiant["answer_live"].values():
                #                 # print(cle_answer) # m'affiche id_question_answered, la date et l'answer de chaque reponse
                #                 # print(cle_answer["id_question_answered"]) # m'affiche tous les id_question_answered
                #                 # print(question_) # me met la question avec l'id de la question
                #                 # les attributs de answer live sont stockes dans l'object question
                #                 question_[
                #                     f"question_{cle_answer['id_question_answered']}"] = cle_answer
                #                 # print(question_[f"question_{cle_answer['id_question_answered']}"])
                #     student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live']}"] = question_
                # print(student)

    def response_good(self, num_etudiant):
        # data_object = {}
        student = {
            f"student_{num_etudiant}": {
                "info_student": {
                    "numero_etudiant": num_etudiant,
                    "nom": None,
                    "prenom": None,
                    "reponse_correct": None,
                },
                "Live": {},

            },
        }

        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(self.link_database_livestream):
                # with open(f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json","r") as file:
                question_ = {}
                with open(f"{self.link_database_livestream}/{file_name}") as file:
                    data = json.load(file)
                    student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"] = {
                    }
                    if data['live_type'] == "QLive":
                        student[f"student_{num_etudiant}"]["info_student"]["reponse_correct"] = self.response_good_by_students(
                            data['id_live'], num_etudiant)
                    elif data['live_type'] == "SeqLive":
                        student[f"student_{num_etudiant}"]["info_student"]["reponse_correct"] = self.response_correcte_pourcentage_seqlive(
                            data['id_live'], num_etudiant)
                    for etudiant in data["espace_students"].values():
                        # print(etudiant)
                        if etudiant["info_student"]["numero_etudiant"] == num_etudiant:
                            student[f"student_{num_etudiant}"]["info_student"]["nom"] = etudiant["info_student"]["nom"]
                            student[f"student_{num_etudiant}"]["info_student"]["prenom"] = etudiant["info_student"]["prenom"]
                            # print(student[f"student_{num_etudiant}"])
                            for cle_answer in etudiant["answer_live"].values():
                                # print(cle_answer) # m'affiche id_question_answered, la date et l'answer de chaque reponse
                                # print(cle_answer["id_question_answered"]) # m'affiche tous les id_question_answered
                                # print(question_) # me met la question avec l'id de la question
                                # les attributs de answer live sont stockes dans l'object question
                                question_[
                                    f"question_{cle_answer['id_question_answered']}"] = cle_answer
                                # print(question_[f"question_{cle_answer['id_question_answered']}"])
                    student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live']}"] = question_
                print(student)

                # print(student) # probleme ca ne me le met pas a la suite et aussi probleme avec num_etudiant et nbr
                # print(student)
                # for attribut in cle_answer["answer_live"].values():
                # for cle_in_cle_answer_live in cle["answer_live"].values():
                #     print(cle_in_cle_answer_live)
                #     student["student_{num_etudiant}"]["Live"]["live_{id_live}"]["question_"]["date"]=cle_in_cle_answer_live["date"]
                #     student["student_{num_etudiant}"]["Live"]["live_{id_live}"]["question_"]["id_question_answered"]=cle_in_cle_answer_live["id_question_answered"]
                #     student["student_{num_etudiant}"]["Live"]["live_{id_live}"]["question_"]["answer"]=cle_in_cle_answer_live["answer"]
                # with open(f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json","w") as files:
                #     json.dump(student,files,indent=4)

                #         file_live["Live"]["live_{id_live}"]["question_"]["date"]=cle_answer_live["date"]
                #         file_live["Live"]["live_{id_live}"]["question_"]["answer"]=cle_answer_live["answer"]
                #         data_object=student
                #         data_object=file_live
                #         print(data_object)

                # k+=1
                # if cle["answer_live"] == true:
                #     i+=1
                # student[f"student_{num_etudiant}"]["info_student"]["reponse_correct"] == i/k

    # def response_good(self,num_etudiant):
    #     data_object={
    #          "numero_etudiant": num_etudiant,
    #          "nom": None,
    #          "prenom": None,
    #          "answer_live":None
    #      }

    #     if (os.path.exists(self.link_database_livestream)):
    #          for file_name in os.listdir(self.link_database_livestream):
    #              data_sous_object={
    #              "Live":None, # problem : number live not ordered at the 3 live
    #              "id_live":None,
    #              "response_correcte":None # si QLive c'est en nombre sinon en pourcentage
    #          }
    #              with open(f"{self.link_database_livestream}/{file_name}") as file:
    #                  data=json.load(file)
    #                  s=0
    #                  for info_data in data["espace_students"].values():
    #                      if (info_data["info_student"]["numero_etudiant"]==num_etudiant):
    #                          s+=1
    #                          data_object["nom"]=info_data["info_student"]["nom"]
    #                          data_object["prenom"]=info_data["info_student"]["prenom"]
    #                          # print(data_object)
    #                          if  data["live_type"]=="QLive":
    #                              data_sous_object["id_live"]=data["id_live"]
    #                              data_sous_object["response_correcte"]=self.response_good_by_students(data["id_live"],num_etudiant)
    #                              data_sous_object["Live"]=s

    #                          elif    data["live_type"]=="SeqLive":
    #                                  data_sous_object["id_live"]=data["id_live"]
    #                                  data_sous_object["response_correcte"]=self.response_correcte_pourcentage_seqlive(data["id_live"],num_etudiant)
    #                                  data_sous_object["Live"]=s
    #                          data_object["answer_live"]=data_sous_object
    #                          s+=1
    #                          print(data_object)
    #                      # print(data_object)    # n'affiche pas le numero du live dans l'ordre a partir du 3eme c'est chaud

    # donne le nombre de reponse total repondu par un etudiant specifique

    def number_response_total_by_one_etudiant(self, id_live, num_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            s = 0
            for student in data["espace_students"].values():
                if student["info_student"]["numero_etudiant"] == num_etudiant:
                    print(
                        student["answer_live"]
                    )  # affiche les reponses de l'etudiant en question
                    for i in student["answer_live"]:
                        s += 1
            return s

    # donne le nombre de reponse total repondu pour tous les etudiants d'un live
    def number_response_total_all_students(self, id_live):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            s = 0
            for student in data["espace_students"].values():
                print(
                    student["answer_live"]
                )  # affiche les reponses de tous les étudiants d'un live
                for i in student["answer_live"]:
                    s += 1
            return s

    # recupere la premiere reponse d'un etudiant specififque(answer0)
    def number_response_correct(self, id_live, numero_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            for student in data["espace_students"].values():
                if student["info_student"]["numero_etudiant"] == numero_etudiant:
                    return student["answer_live"]["answer0"]

    # donne le nombre de reponse correcte en pourcentage pour un etudiant pour seqlive /ca marche
    def response_correcte_pourcentage_seqlive(self, id_live, numero_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            response_correct = 0
            pourcentage = 0
            for student in data:
                if data["live_type"] == "SeqLive":
                    for students in data["espace_students"].values():
                        if (
                            students["info_student"]["numero_etudiant"]
                            == numero_etudiant
                        ):
                            for donnee_answer_live in students["answer_live"].values():
                                if donnee_answer_live["answer"] == True:
                                    response_correct += 1
                                    pourcentage = response_correct / len(
                                        students["answer_live"]
                                    )
                return str(round(pourcentage * 100,2)) + "%"  # retourne en pourcentage
            # la fonction round arrondi a l'entier superieur si c'est superieur a la moitie,sinon a l'entier inferieur

    # ca vide les reponses des etudiants pour un qlive/seqlive
    def effacer_response(self, id_live):
        if os.path.exists(self.link_database_livestream):
                with open(f"{self.link_database_livestream}/{id_live}_Data.json", "r") as file:
                    data = json.load(file)  # c'est memorise en memoire, retourne un objet json
                    for student in data["espace_students"]:
                        # print(student)
                        data["espace_students"][student]["answer_live"] = {}
                        
                        with open(f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json","w",) as file_response:
                            json.dump(data, file_response, indent=4)

    # json.dump(data,file)sa creer le fichier json et ecrit les donnees si il le trouve pas sinon il va dans le fichier json et l'ecrit les donnees

    def response_seqlive_by_etudiant(self, id_live, user_name, numero_etudiant):
        self.inscrire_all_student_live_json(
            self.user_name, id_live, numero_etudiant)
        if os.path.exists(
            f"./components/LiveStream_database/all_student_connected_live.json"
        ):
            with open(
                "./components/LiveStream_database/all_student_connected_live.json", "w"
            ) as files:
                data_all_student_connected = json.load(files)
                for i in data_all_student_connected:
                    data_all_student_connected["numero_etudiant"] == numero_etudiant
                    with open(
                        f"./components/LiveStream_database/{data_all_student_connected[i][user_name]}_Data_LIVE/{id_live}_Data.json",
                        "r",
                    ) as file:
                        data_user_name = json.load(file)
                        s = 0
                        for i in data_user_name:
                            if data_user_name[i]["answer"] == "True":
                                s += 1
                            s = i / len(data_user_name)
                    return str(round(s * 100)) + "%"
                print(s)

    # def comparer_dates(cle_sous_objet):
    #     return [cle_sous_objet]["date_live_stream"]

    def data_participant(self):
        data = {}
        # url="./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json"
        # link_database_livestream="./components/LiveStream_database/raphael_Data_LIVE"
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(
                self.link_database_livestream
            ):  # listdir : donne un tableau ou il y aura les noms de chemin de chaque fichier

                print("--------")
                data_object = {
                    "id_live": None,
                    "date_live_stream": None,
                    "number_etudiant": None,
                }

                with open(f"{self.link_database_livestream}/{file_name}") as file:
                    data_live = json.load(file)
                    data_object["id_live"] = data_live["id_live"]
                    data_live["date_live_stream"]
                    data_object["date_live_stream"] = str(
                        datetime.strptime(
                            data_live["date_live_stream"], "%Y-%m-%d %H:%M:%S.%f"
                        ).strftime("%-d %B %Y à %H:%M:%S")
                    )
                    data_object["number_etudiant"] = self.number_etudiant(
                        data_object["id_live"]
                    )
                    data[file_name] = data_object

        # ! Trie des données avant d'etre retourner
        def compare_dates(key_obj):
            return data[key_obj]["date_live_stream"]

        # Tri des clés de sous-objet par date
        key_sorted = sorted(data.keys(), key=compare_dates)
        # Création d'un nouveau dictionnaire trié à partir du dictionnaire original
        data_sorted = {cle: data[cle] for cle in key_sorted}
        return data_sorted

    def number_etudiant(self, id_live):
        if os.path.exists(self.link_database_livestream):  # c'est dynamique
            with open(
                f"{self.link_database_livestream}/{id_live}_Data.json", "r"
            ) as file:  # f permet de mettre des variables (ici qui sont de types string)
                data = json.load(file)
                s = 0
                for students in data["espace_students"]:
                    s += 1
                return s

    # def envoi():
    #     b = Statistique("raphael")
    #     emit("envoi_object", b, broadcast=True)


# b = Statistique("raphael")
# b.effacer_response("SeqLive - d3a09081")
# print(b._give_stat_Student("22004105"))
# SocketIO.emit('object',b)
# b.envoi()  # surtout le laisser comme ca ne pas le mettre en commentaire
# c=b.number_response_by_students("qLive - b0c9088b","22101635")
# c=b.number_response_total("qLive - b0c9088b","22101633")
# c=b.response_correcte_pourcentage_seqlive("SeqLive - 3accd738","22101635")
# print(c)
# f=b.effacer_response("qLive - a575a98b")
# f=b.response_good_by_students("qLive - a575a98b","22101635")
# f=b.response_correcte_pourcentage_seqlive("SeqLive - 41ae7d68","22101635")
# print(f)
# b=Statistique("fred")
# # c=b.number_response_by_students("qLive - b0c9088b","22101635")
# # c=b.number_response_total("qLive - b0c9088b","22101633")
# # c=b.response_correcte_pourcentage_seqlive("SeqLive - 3accd738","22101635")
# # print(c)
# # f=b.effacer_response("qLive - a575a98b")
# # f=b.response_good_by_students("qLive - a575a98b","22101635")
# f = b.response_good1("22101635")
# f=b.effacer_response("SeqLive - 92df1bfe")
# f=b.response_correcte_pourcentage_seqlive("SeqLive - 92df1bfe","22101635")
# print(f)
