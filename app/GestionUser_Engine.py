import json
import os
from __Module_utility_Function__ import add_object_to_json
from _Enseignant import Enseignant
from _Etudiant import Etudiant

from spellchecker import SpellChecker
spell_fr = SpellChecker(language='fr')
spell_en = SpellChecker(language='en')
import spacy
# spacy_dico = spacy.load("fr_core_news_lg")
spacy_dico = spacy.load("en_core_web_sm")


# à supprimer : Enonce ( Id ) : 7ac7a34e



#à installer dans le terminal:
#pip install pyspellchecker
#pip install spacy
#et enfin:  python -m spacy download fr_core_news_sm



class GestionUser:
    # STOCKE USER_ONLINE
    userConnected_ = {"enseignant_connected": [], "etudiant_connected": []}
    links_DataBase_User = {
        "enseignant": "./components/user_info/all_user_password.json",
        "etudiant": "components/user_info/students_DataBase.json",
    }
    # Ajouter dans le serveur un utilisateur connéctée
    """
    ? Garder en memoire les infos de chaque utilisateur connéctée en instanciant un objet de type (User) -> ["Enseignant","Etudiant"]
    """
    # ? Un live existe avec l'id passsée en parametre tu renvoie l'object de type live utilise
    # ? la methode participer a un live de gestion user gestion user utilise la methode connect du live et lui envoie l'instance de etudiant
    # ! Une methode participer a un live avec l'instance de user_etudiant
    def _login_User(self, u_name, u_password, type_user):
        if self.check_userAccount(u_name, u_password, type_user) == True:
            if type_user == "enseignant":
                #il ajoute une instanciation d'Enseignant dans l'objet cle enseignant dans le tableau des connecter.
                self.userConnected_["enseignant_connected"].append(
                    Enseignant(u_name, u_password)
                )
                # print(self.userConnected_["enseignant_connected"])
                return True
            elif type_user == "etudiant":
                _user = self._getInfoEtudiant_Not_Connected(u_name)  # c'est le numero etudiant
                self.userConnected_["etudiant_connected"].append(
                    Etudiant(_user["nom"],_user["prenom"],_user["numero_etudiant"])
                )
                # print(self.userConnected_["etudiant_connected"])
                return True
        return False
    # Stat user
    def _numberEtudiant_connected(self):
        return len(self.userConnected_["etudiant_connected"])
    def _numberEnseignant_connected(self):
        return len(self.userConnected_["enseignant_connected"])

    # ! function(type(bool)) -> Verifie si u_name & u_mdp est dans la base de donnée en fonction typeUSer
    def check_userAccount(self, user_name, user_password, type_user):
        with open(self.links_DataBase_User[type_user], "r") as dataBase:
            buffer_read = json.load(dataBase)
            if type_user == "enseignant":
                for key in buffer_read:
                    if (
                        buffer_read[key]["user_name"] == user_name
                        and buffer_read[key]["user_password"] == user_password
                    ):
                        return True
            elif type_user == "etudiant":
                for key in buffer_read:
                    if (
                        buffer_read[key]["numero_etudiant"] == user_name
                        and buffer_read[key]["mot_de_passe"] == user_password
                    ):
                        return True

    # ! Inscription d'un Enseignant
    def registration_Enseignant(self, u_name, u_password):
        object_User_formated = {"user_name": u_name, "user_password": u_password}
        add_object_to_json(
            object_User_formated, "enseignant", self.links_DataBase_User["enseignant"]
        )
        # Création des fichier de l'enseigant
        if (
            os.path.exists(f"./components/user_info/Etiquete/{u_name}_etiquette.json")
            == False
        ):
            with open(
                f"./components/user_info/Etiquete/{u_name}_etiquette.json", "w"
            ) as file_:
                json.dump({}, file_)
        if (
            os.path.exists(
                f"./components/user_info/Question_DataBase/{u_name}_question.json"
            )
            == False
        ):
            with open(
                f"./components/user_info/Question_dataBase/{u_name}_question.json", "w"
            ) as file_:
                json.dump({}, file_)

        # Création du dossier liveSTream de chaque enseignant
        if (os.path.exists(f"./components/LiveStream_database/{u_name}_Data_LIVE") == False):
            os.mkdir(f"./components/LiveStream_database/{u_name}_Data_LIVE")

    def get_user(self, u_name, type_user):
        
        if type_user == "enseignant":
            for user_enseignant in self.userConnected_["enseignant_connected"]:
                if user_enseignant.user_name == u_name:
                    return user_enseignant
        elif type_user == "etudiant":
            for user_etudiant in self.userConnected_["etudiant_connected"]:
                if user_etudiant.numero_etudiant == u_name:
                    return user_etudiant

    def _getInfoEtudiant_Not_Connected(self, num_etudiant):
        with open(self.links_DataBase_User["etudiant"], "r") as dataBase:
            buffer_read = json.load(dataBase)
            for student in buffer_read:
                if buffer_read[student]["numero_etudiant"] == num_etudiant:
                    return buffer_read[student]
                
    def _disconnected(self,type_user_,user_name):
        if (type_user_ == "enseignant"):
            for user_enseignant in self.userConnected_["enseignant_connected"]:
                if user_enseignant.user_name == user_name:
                    self.userConnected_["enseignant_connected"].pop()
        elif type_user_ == "etudiant":
            for user_etudiant in self.userConnected_["etudiant_connected"]:
                if user_etudiant.info_students["numero_etudiant"] == user_name:
                    self.userConnected_["etudiant_connected"].pop()

#Method LIVE:

    #Verifie Live:
    def _getLive_instance(self,id_live):
        for enseignant in self.userConnected_["enseignant_connected"]:
            if enseignant._current_LIVE_STREAM.id_uLive == id_live:
                return enseignant._current_LIVE_STREAM
        return None

    def Verif_code_acces_Live(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"]:
            if enseignant._current_LIVE_STREAM != None:
                if enseignant._current_LIVE_STREAM.id_uLive == _code_acces:
                    return True
        return False
    
    def get_InFo_Live_enonce(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                    return enseignant._current_LIVE_STREAM.question_live_content.Enonce 
        return None

   
    def get_InFo_Live_propositions(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                    return enseignant._current_LIVE_STREAM.question_live_content.Q_proposition  
        return None

    def get_Info_Live_objet(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                    #return json.dumps(enseignant._current_LIVE_STREAM.question_live_content)
                    return enseignant._current_LIVE_STREAM.question_live_content.getJson_info()
        return None


    #je suis ici dernier modification  c'est lui qui renvoie le tableau avec ajout des proposition QO
    def Ajout_proposition_Live_QO(self,_code_acces,reponses):
        for enseignant in self.userConnected_["enseignant_connected"] :
            if enseignant._current_LIVE_STREAM != None:
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    print(enseignant._current_LIVE_STREAM.question_live_content.Q_proposition)
                    if enseignant._current_LIVE_STREAM.question_live_content.type_question == 'QO':
                        if (reponses != []):

                            reponse_QO = self.correcteur(self.get_InFo_Live_propositions(_code_acces),self.find_word(reponses[0]))
                            # enseignant._current_LIVE_STREAM.question_live_content.Q_proposition.append(reponses[0])
                            enseignant._current_LIVE_STREAM.question_live_content.Q_proposition.append(reponse_QO)
                            print(enseignant._current_LIVE_STREAM.question_live_content.Q_proposition)



    # pour (seqLive et qLive)
    def get_Type_LIve(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                    return enseignant._current_LIVE_STREAM.type_live
        return None
        
    
    def get_verif_reponse(self,reponses,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive :
                    #print(enseignant._current_LIVE_STREAM.question_live_content.response_is_correct(reponses))
                    #print(reponses[0])
                    #print(reponses)
                    if reponses != [] and enseignant._current_LIVE_STREAM.type_live == "QLive" and  enseignant._current_LIVE_STREAM.question_live_content.response_is_correct(reponses):
                        return True
                    if reponses != [] and enseignant._current_LIVE_STREAM.type_live == "SeqLive" and  enseignant._current_LIVE_STREAM._current_questionLive.response_is_correct(reponses):
                        return True
        return False

    def Ajouter_reponse_objet(self,_code_acces,num_etudiant,reponses):
        print("Je suis entrée dans la fonciton answer")

        # if reponses[0]== '':
        #     reponses.pop(0)
        for enseignant in self.userConnected_["enseignant_connected"]:
                if enseignant._current_LIVE_STREAM != None:
                    print("Je suis dans l'etage")
                    if _code_acces == enseignant._current_LIVE_STREAM.id_uLive and reponses != [] :
                        print("J'appel la fonciton answer")
                        #enseignant._current_LIVE_STREAM._answer_question(num_etudiant,reponses[1:len(reponses)]) #ne jamais oublier le self pour utiliser une method public
                        enseignant._current_LIVE_STREAM._answer_question(num_etudiant,reponses[1:])

    def quitter_live_enCours(self,_code_acces,num_etudiant):
        for enseignant in self.userConnected_["enseignant_connected"]:
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    enseignant._current_LIVE_STREAM._quit_Live(num_etudiant)


    #   SeqLive:
    
    def get_InFo_Live_enonce_seqLive(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
            if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                return enseignant._current_LIVE_STREAM._current_questionLive.enonce
        return None
    
    def get_InFo_Live_propositions_seqLive(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
            if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                return enseignant._current_LIVE_STREAM._current_questionLive.Q_proposition  
        return None

    def get_Info_Live_objet_seqLive(self,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
            if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                #leçon du jour un tableau est comme une liste en python puisque on a fait [] = {} donc on agit avec une list après sa
                #return json.dumps(enseignant._current_LIVE_STREAM.question_live_content)
                return enseignant._current_LIVE_STREAM._current_questionLive.getJson_info()
        return None

    def get_verif_reponse_seqLive(self,reponses,_code_acces):
        for enseignant in self.userConnected_["enseignant_connected"] :
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive :
                    if reponses != [] and enseignant._current_LIVE_STREAM._current_questionLive.response_is_correct(reponses):
                        return True
        return False
    
    def Ajout_proposition_seqLive_QO(self,_code_acces,reponses):
        for enseignant in self.userConnected_["enseignant_connected"] :
            if enseignant._current_LIVE_STREAM != None:
                if _code_acces == enseignant._current_LIVE_STREAM.id_uLive:
                    #print(enseignant._current_LIVE_STREAM.question_live_content.Q_proposition)
                    if enseignant._current_LIVE_STREAM._current_questionLive.type_question == 'QO':
                            reponse_QO = self.correcteur(self.get_InFo_Live_propositions_seqLive(_code_acces),self.find_word(reponses[0]))
                            # enseignant._current_LIVE_STREAM._current_questionLive.Q_proposition.append(reponses[0])
                            enseignant._current_LIVE_STREAM._current_questionLive.Q_proposition.append(reponse_QO)
                            print(enseignant._current_LIVE_STREAM._current_questionLive.Q_proposition)
                            return enseignant._current_LIVE_STREAM._current_questionLive.Q_proposition

    def find_word(self,mot):
        print("fw: je suis au debut ")
        print(mot)
        doc = spacy_dico(mot)

        # if mot in spacy_dico.vocab:
        #     print("je suis rentrer dans spacy")
        #     return mot

        for word in spell_fr:
            if word == mot:
                print("fw: vrai je suis ici : fr")
                print(word)
                return word
        for word in spell_en:
            if word == mot:
                print("fw: vrai je suis ici : en")
                print(word)
                return word
            
        if doc.has_vector:
            print("je suis dans has_vector")
            print(doc.text)
            return doc.text
            # return mot
        
        mot_tester = spell_fr.correction(mot)
        if mot_tester == None:
            print("fw: ici programme find_word:")
            print(mot)
            return mot
        else:
            print("fw: je suis dans le else:")
            print(mot_tester)
            return mot_tester
        

    def correcteur(self,tableau_propositions_,reponse_QO):
        if tableau_propositions_ == []: 
            return reponse_QO
        else:
            for propositions in tableau_propositions_ :
                if reponse_QO.lower() == propositions.lower():
                    return propositions
                else: 
                    tab_str_rep = list(reponse_QO)
                    tab_str_pro = list(propositions)

                    NB = 0
                    n = len(tab_str_rep)
                    l = len(tab_str_pro)
                    if n <= l: taille = l
                    else: taille = n

                    i = 0
                    while i < n and i < l:
                        if tab_str_rep[i].lower() == tab_str_pro[i].lower():
                            NB += 1
                        i += 1
                    if 60 <= (NB / taille) * 100:
                        return propositions
        return reponse_QO
                