
let fieldCount = 0;
// import { jsPDF } from "jspdf";
function removeQuestion(button) {
	//Récupérer le conteneur parent de tous les champs
	var container = document.getElementById("container_questions");
	//Récupérer le champ à supprimer en remontant dans l'arbre DOM
	var field = button.parentNode;
	//Supprimer le champ de la page
	container.removeChild(field);
}

function addQuestion() {
	//Créer un nouveau élément de saisie
	var input = document.createElement("input");
	input.name = "question" + fieldCount;
	input.id = "question" + fieldCount
	input.type = "text";
	input.classList.add("form-control", "proposition", "question")
	//Créer un nouveau élément de case à cocher
	var checkbox = document.createElement("input");
	checkbox.type = "checkbox";
	checkbox.id = "question_true" + fieldCount
	checkbox.name = "question_true" + fieldCount;
	checkbox.classList.add("form-check-input", "proposition_checked")
	// checkbox.value = document.getElementById  
	//Créer un bouton de suppression
	var button = document.createElement("button");
	button.innerHTML = "Supprimer";
	button.classList = "input-group-text"
	button.onclick = function() {
		removeQuestion(this);
	};

	//Créer un conteneur pour le champ et le bouton
	var container = document.createElement("div");
	container.classList.add("question_container")
	container.appendChild(checkbox);
	container.appendChild(input);
	container.appendChild(button);

	//Ajouter le conteneur à la page
	document.getElementById("container_questions").appendChild(container);


	//Incrémenter le compteur de champs créés
	checkbox.value = input.value

	fieldCount++;
}


function removeThematique(button) {
	//Récupérer le conteneur parent de tous les champs
	var container = document.getElementById("container_thematique");
	//Récupérer le champ à supprimer en remontant dans l'arbre DOM
	var field = button.parentNode;
	//Supprimer le champ de la page
	container.removeChild(field);
}

function addThematique() {
	var input = document.createElement("input");
	input.name = "thematique" + fieldCount;
	input.type = "text";
	input.classList.add("form-control", "thematique")

	//Créer un bouton de suppression
	var button = document.createElement("button");
	button.innerHTML = "Supprimer";
	button.classList = "input-group-text"
	button.onclick = function() {
		removeThematique(this);
	};

	//Créer un conteneur pour le champ et le bouton
	var container = document.createElement("div");
	container.appendChild(input);
	container.appendChild(button);

	//Ajouter le conteneur à la page
	document.getElementById("container_thematique").appendChild(container);


	//Incrémenter le compteur de champs créés
	fieldCount++;

}


// Question 

function SendQuestion() {
	let all_proposition = document.querySelectorAll(".proposition")
	let info_question_formated = {
		enonce: document.getElementById("enonce_champ").value,
		type_question: document.querySelector(".choice_typeQUestion").value,
		propositions: [],
		reponses: [],
		thematique: [],
	}
	


	//  ! L'ajout des reponses coché
	// Récupération de tous les éléments de type "checkbox" cochés avec la classe "my-class"
	let checkedBoxes = document.querySelectorAll('input.proposition_checked[type="checkbox"]:checked');
	// -> Affectation des checkbox a une value
	for (let i = 0; i < all_proposition.length; i++) {
		document.querySelectorAll('input.proposition_checked[type="checkbox"]')[i].value = all_proposition[i].value
	}
	for (let i = 0; i < checkedBoxes.length; i++) {
		info_question_formated["reponses"].push(checkedBoxes[i].value)
	}

	// -- Ajout des thématiques de l'utilisateur si checked
	let thematique_checked = document.querySelectorAll('input.thematique_checked[type="checkbox"]:checked');
	for (let i = 0; i < thematique_checked.length; i++) {
		if (thematique_checked[i].checked) {
			info_question_formated["thematique"].push(thematique_checked[i].value);
		}
	}
	let all_thematique = document.querySelectorAll("input.thematique")
	// ! Ajout de tout les thématiques
	for (let i = 0; i < all_thematique.length; i++) {
		info_question_formated["thematique"].push(all_thematique[i].value)
	}
	// ! Ajout de tout les propositions
	for (let i = 0; i < all_proposition.length; i++) {
		info_question_formated["propositions"].push(all_proposition[i].value)
	}
	document.getElementById("add_question").value = JSON.stringify(info_question_formated)
	let form_addQuestion = document.getElementsByClassName("add_question")
	// form_addQuestion.submit()
}




