function addQuestion() {
    //Créer un nouveau élément de saisie
    var input = document.createElement("input");
    input.name = "question" + fieldCount;
    input.id = "question" + fieldCount
    input.type = "text";
    input.classList.add("form-control","proposition")
    //Créer un nouveau élément de case à cocher
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.id = "question_true" + fieldCount
    checkbox.name = "question_true" + fieldCount;
    checkbox.classList.add("form-check-input","proposition_checked")
    // checkbox.value = document.getElementById  
    //Créer un bouton de suppression
    var button = document.createElement("button");
    button.innerHTML = "Supprimer";
    button.classList = "input-group-text"
    button.onclick = function() { removeQuestion(this); };
    
    //Créer un conteneur pour le champ et le bouton
    var container = document.createElement("div");
    container.appendChild(input);
    container.appendChild(checkbox);
    container.appendChild(button);
    
    //Ajouter le conteneur à la page
    document.getElementById("container_questions").appendChild(container);
  
  
    //Incrémenter le compteur de champs créés
    fieldCount++;
  }