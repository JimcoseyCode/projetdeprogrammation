function preparation_traduction(enonce_recu){
    let objet_de_traduction = {"mermaid":null,"highlight":null,"markdown":null,"latex_mathjax":null,"partie_normal":null};
    

    const expReg_Markdown = /^#.*/gm;
    let partiesMarkdown = enonce_recu.match(expReg_Markdown);
    objet_de_traduction["markdown"] = partiesMarkdown;
    const expReg_Latex = /\$\$.*?\$\$/g;
    let partiesLatex = enonce_recu.match(expReg_Latex);
    objet_de_traduction["latex_mathjax"] = partiesLatex;
    const expReg_Highlight = /```.*\n([\s\S]*?)```/g;
    let partiesHighlight = enonce_recu.match(expReg_Highlight);
    objet_de_traduction["highlight"] = partiesHighlight;
    console.log(JSON.stringify(objet_de_traduction) + " icif1");
    
    var normalParts_TAB = [];
    var normalParts = enonce_recu;
    // La méthode .test() retourne true si la sous-chaîne est trouvée et false sinon (selons si il a trouver l'exrpession regulier dans l'enoncer)
    var verite_1=false,verite_2=false,verite_3=false;

    if(expReg_Markdown.test(enonce_recu)){normalParts = enonce_recu.replace(expReg_Markdown,'');verite_1 = true;}
    if(expReg_Latex.test(enonce_recu)){normalParts = normalParts.replace(expReg_Latex, '');verite_2 = true;}    
    if(expReg_Highlight.test(enonce_recu)){normalParts = normalParts.replace(expReg_Highlight, '');verite_3 = true;}    
    if(verite_1==true || verite_2==true || verite_3==true){normalParts_TAB.push(normalParts);}
    else{normalParts_TAB.push(enonce_recu);}
    objet_de_traduction["partie_normal"] = normalParts_TAB;
    
    console.log(JSON.stringify(objet_de_traduction) + " icif2");


    let partiesMermaid = [];
    if(partiesHighlight != null){
        for(let element of partiesHighlight){
            if(element.includes("mermaid")){
                partiesMermaid.push(element);
                let index_element = partiesHighlight.indexOf(element);
                partiesHighlight.splice(index_element,1);// enleve mermaid de la partie hightlight
        }}
    }
        

    objet_de_traduction["mermaid"] = partiesMermaid; 

    return objet_de_traduction;
}


function traduction_mermaid(enonce_mermaid){
        //mermeid 
        let pre = document.createElement("pre");
        pre.className = "mermaid";
        let code = document.createElement("code");

        enonce_mermaid = enonce_mermaid.replace(/```|mermaid/gi,"")
        code.textContent = enonce_mermaid;
        pre.appendChild(code);
        document.getElementById("ici_1").appendChild(pre);

    document.querySelectorAll("pre.mermaid, pre>code.language-mermaid").forEach($el => {
    if ($el.tagName === "CODE")
        $el = $el.parentElement
    $el.outerHTML = `<div class="mermaid">${$el.textContent}</div>`
    })
    mermaid.initialize({
        logLevel: "error", // [1]
        securityLevel: "loose", // [2]
        theme: (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) ?
            "dark" :
            "default" // [3]
    })

}

function traduction_highlight(enonce_highlight){
    //hightlight
    let pre = document.createElement("pre");
    let code = document.createElement("code");
    enonce_highlight = enonce_highlight.replace(/```[^`\n]*\n|```/g,"") 
    console.log(enonce_highlight);
    code.textContent = enonce_highlight;
    pre.appendChild(code);
    pre.id = "ici_"
    document.getElementById("ici_1").appendChild(pre);
    code_highlight = document.getElementById("ici_")
    hljs.highlightBlock(code_highlight);
}

function traduction_markdown(enonce_markdown){
    //markdown
        const div_ = document.createElement("div");
        let converter = new showdown.Converter();
        let text = enonce_markdown;
        div_.innerHTML= converter.makeHtml(text);
        document.getElementById("ici_1").appendChild(div_);
}

    function traduction_latex_mathjax(enonce_latex){
        //Latex mathjax
        const div = document.createElement('div');
        div.innerHTML = enonce_latex;
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, div]);
        document.getElementById("ici_1").appendChild(div);
    }

function traduction_normal(enonce_normal){
    const div = document.createElement('div');
    const h2 = document.createElement('h2');
    h2.textContent = enonce_normal;
    div.appendChild(h2);
    document.getElementById("ici_1").appendChild(div);
}


function traduction_to_affiche(){
    var enonce = JSON.parse(document.getElementById("enonce").value);
    var objet_traduction = preparation_traduction(enonce);
    if(objet_traduction["mermaid"] != null && objet_traduction["mermaid"] != []){
            for(let element of objet_traduction["mermaid"]){
                traduction_mermaid(element);
            }
        }

    if(objet_traduction["highlight"] != null && objet_traduction["highlight"] != []){
            for(let element of objet_traduction["highlight"]){
                traduction_highlight(element);
            }
        }               
            
    if(objet_traduction["markdown"] != null && objet_traduction["markdown"] != []){
            for(let element of objet_traduction["markdown"]){
                    traduction_markdown(element);
            }
        }
    setTimeout(function() {
        if(objet_traduction["latex_mathjax"] != null && objet_traduction["latex_mathjax"] != []){
        for(let element of objet_traduction["latex_mathjax"]){       
                    traduction_latex_mathjax(element);
                }
            }
    }, 500);

    if(objet_traduction["partie_normal"] != null && objet_traduction["partie_normal"] != []){
        for(let element of objet_traduction["partie_normal"]){
                traduction_normal(element);
            }
        }
}



mermaid.initialize({
    startOnLoad: true
});

function renderMermaid() {
    mermaid.init(undefined, document.querySelectorAll(".language-mermaid"));
}

let visual_engine = () => {

    // Obtenir le code HTML généré
    for (let i = 0; i < document.getElementsByClassName("enonce_champ").length; i++) {
        let enonce_value = document.getElementsByClassName("enonce_champ")[i]
        document.getElementsByClassName("visual_data")[i].innerHTML = marked.parse(enonce_value.value)
    }
    renderMermaid();
    document.querySelectorAll('pre code').forEach((el) => {
        hljs.highlightElement(el);
    });
}

function quittCorrection(){
    document.querySelector(".correction_section").style["display"] = "none"
    document.querySelector(".main_").style["display"] = "block"
}
       


// function button_checked(type_question){
// ///cas des checkbox///
// if (type_question == 'qcm'){
//     var cases = document.querySelectorAll("input[type='checkbox']"); //sa cree un tableau des checkbox
//     var valeursCases = [];
//     for (var i = 0; i < cases.length; i++) {
//     var caseCourante = cases[i];
//         if (caseCourante.checked) {
//             valeursCases.push(caseCourante.name);
//         }
//     }
//     document.getElementById("reponses").value = JSON.stringify(valeursCases);
// }
// else if(type_question == 'QN'){
// ///cas des question_numerique///
//     var qcu = document.getElementById("reponse_QN").value;
//     var valeursInput = [];
//     valeursInput.push(qcu);
//     document.getElementById("reponses").value = JSON.stringify(valeursInput);
//     //console.log(document.getElementById("reponses").value );
// }
// else if(type_question == 'qcu'){
//     //var cases = document.querySelectorAll("input[type='checkbox']"); //sa cree un tableau des checkbox
//     var checkboxes = document.querySelectorAll('input[type=checkbox]');
//     var valeursCases = [];
//     for (var i = 0; i < checkboxes.length; i++) {
//     var caseCourante = checkboxes[i];
//         if (caseCourante.checked) {
//             valeursCases.push(caseCourante.name);
//         }
//     }
//     document.getElementById("reponses").value = JSON.stringify(valeursCases);
// }
// else{
//     console.log("erreur");
// }
// }


