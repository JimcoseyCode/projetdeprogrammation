// Fonction -> donne tous les id de chaque question cochée
// const socket = io();
// import io from "socket.io-client";
let array_File_id_question = []
let catch_all_question_ID = () => {
    let result_stock_idQuestion = array_File_id_question
    // let question_selected = document.querySelectorAll('input.question_check__[type="checkbox"]:checked')
    // for (let i = 0; i < question_selected.length; i++) {
    //     result_stock_idQuestion.push(question_selected[i].value)
    // }

    return result_stock_idQuestion
}
let start_live = () => {
    let object_live_stream = {
        "live_type": document.querySelector(".type_live").value,
        // Tout les questions de la diffusion du live
        "Q_id_selected": catch_all_question_ID()
    }
    console.log(object_live_stream);
    document.querySelector("#settings_data_live").value = JSON.stringify(object_live_stream)

};
let redirect_dashboard = () => {
    url = window.location.href.split("/")
    window.location.replace("/Live/" + url[4] + "/settings")
}
let redirect_dashboard_enseignant = () => {
    url = window.location.href.split("/")
    window.location.replace(url[0] + "/dashboard/enseignant/" + url[url.length - 2])
}


let catch_checked_File = (id_question) => {
    array_File_id_question.push(id_question)
}
let remove_id = (current_btn) => {
    if (current_btn.checked == false) {
        console.log(current_btn.checked);
        console.log(current_btn.value);
        let array_result = []
        for (let i = 0; i < array_File_id_question.length; i++) {
            if (array_File_id_question[i] != current_btn.value) {
                array_result.push(array_File_id_question[i])
            }
        }
        array_File_id_question = array_result

    }
}


// ! :: ->  Fonctionalités Livestream




// console.log(socket);

let print_answerLive = () => {
    let chart_answerLive = document.querySelector(".chart_divAnswer")
    // Stoppée la soumission de question dans le live
    if (chart_answerLive.style["display"] == "none") {
        chart_answerLive.style["display"] = "flex"
    }
    else {
        chart_answerLive.style["display"] = "none"
    }
}
// chart_answerLive.onclic
// chart_answerLive.addEventListener("click", () => {
//     if (chart_answerLive.style["display"] == "none"){
//         chart_answerLive.style["display"] = "flex"
//     }
//     else{
//         chart_answerLive.style["display"] = "none"
//     }
// })
// [Tools] => LiveSTream
// ----> Hide enonce
let hide_enonce = () => {
    let btn_hide = document.querySelector('.hide_enonce')
    // let states_enonce = NaN
    if (btn_hide.checked) {
        document.querySelector(".visual_data").style["display"] = "none"

        // states_enonce = true
    }
    else if (btn_hide.checked == false) {
        document.querySelector(".visual_data").style["display"] = "grid"
    }
    // // envoie l'état de l'enonce
    // socket.emit('hide_enonce', {"states_enonce":states_enonce});

}
// ----> Hide answer
let hide_answer = () => {
    let btn_hide = document.querySelector('.hide_answer')
    if (btn_hide.checked) {
        document.querySelector(".propositions_section").style["display"] = "none"
    }
    else if (btn_hide.checked == false) {
        document.querySelector(".propositions_section").style["display"] = "grid"
    }
}







// let socket = io.connect('http://' + document.domain + ':' + location.port);
// // f -> nbr_user_connected
// socket.on('stat_user_connected', function (participants) {
//     // Met à jour le contenu de la page avec la liste des participants
//     document.querySelector('.nbr_user_connected').innerHTML = participants["nbr_user_connected"];
// });


// socket.emit('giveData_answer_Livestream', document.querySelector(".IdLive_").value)
// socket.on('giveData_answer_Livestream', function (data_) {
//     console.log(data_);
//     const ctx = document.getElementById('stat_graphic');

//     new Chart(ctx, {
//         type: 'doughnut',
//         data: {
//             labels: Object.keys(data_).map(id => id),
//             datasets: [{
//                 // label: '# of Votes',
//                 data: Object.values(data_).map(id => id + 4),
//                 backgroundColor: [
//                     'rgba(255, 99, 132, 0.2)',
//                     'rgba(255, 159, 64, 0.2)',
//                     'rgba(255, 205, 86, 0.2)',
//                     'rgba(75, 192, 192, 0.2)',
//                     'rgba(54, 162, 235, 0.2)',
//                     'rgba(153, 102, 255, 0.2)',
//                     'rgba(201, 203, 207, 0.2)'
//                 ],
//                 borderColor: [
//                     'rgb(255, 99, 132)',
//                     'rgb(255, 159, 64)',
//                     'rgb(255, 205, 86)',
//                     'rgb(75, 192, 192)',
//                     'rgb(54, 162, 235)',
//                     'rgb(153, 102, 255)',
//                     'rgb(201, 203, 207)'
//                 ],
//                 borderWidth: 1,
//                 hoverOffset: 4
//             }]
//         },
//         options: {

//         }
//     });
//     // Met à jour le nombre d'etudiant ayant soumis une reponses
//     // document.querySelector('.nbr_user_answered').innerHTML = data_["nbr_user_answered"];
// });
// // f -> nbr_user_answered
// socket.on('stat_user_answered', function (data_) {
//     console.log(data_);
//     // Met à jour le nombre d'etudiant ayant soumis une reponses
//     document.querySelector('.nbr_user_answered').innerHTML = data_['nbr_user_answered'];
// });

// let stop_answer = () => {
//     let id_live = document.querySelector(".IdLive_").value
//     console.log("jarret les reponses");
//     // Stoppée la soumission de question dans le live
//     socket.emit("stop_answer", id_live)
// }