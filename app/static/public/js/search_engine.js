// Fonction -> donne tous les id de chaque question cochée
let catch_all_question_ID = () => {
    let result_stock_idQuestion = []
    let question_selected = document.querySelectorAll('input.question_check__[type="checkbox"]:checked')
    for (let i = 0; i < question_selected.length; i++) {
        result_stock_idQuestion.push(question_selected[i].value)
    }
    return result_stock_idQuestion
}
let generate_page = () => {
    let object_page_ = {
        "title": document.getElementsByClassName("name_page")[0].value,
        "Q_id_selected": catch_all_question_ID()
    }
    document.querySelector("#show_page").value = JSON.stringify(object_page_)
    console.log(document.querySelector("#show_page").value);
    // btn_s = document.getElementsByClassName("generate_page")
    // btn_s.type = "submit"
    // btn_s.submit()
};
let _search_by_thematique = () => {
    let array_selected_check = []
    let thematique_checked = document.querySelectorAll('input.thematique_checked[type="checkbox"]:checked');
    for (let i = 0; i < thematique_checked.length; i++) {
        if (thematique_checked[i].checked) {
            array_selected_check.push(thematique_checked[i].value);
        }
    }
    return array_selected_check

}

let catch_all_thematique_selected = () => {
    obj_ = {
       
    }
    let thematique_selected = document.querySelectorAll('input.thematique_check[type="checkbox"]:checked')
    for (let i = 0; i < thematique_selected.length; i++) {
        obj_[thematique_selected[i].value] = {
            "min": parseInt(document.querySelector("#min_" + thematique_selected[i].value).value),
            "max":parseInt(document.querySelector("#max_" + thematique_selected[i].value).value)
        }
    }
    return obj_
}
let generate_controle = () => {
    nbr_questions
    document.querySelector(".nbr_questions").value = document.querySelector("#nbr_questions").value
    document.querySelector("#generate_controle_i").value =  JSON.stringify(catch_all_thematique_selected())
    document.querySelector(".nbr_controle").value =  document.querySelector("#nbr_sujets").value
    document.querySelector("#type_order_s").value =document.querySelector(".type_of_sujet").value


}