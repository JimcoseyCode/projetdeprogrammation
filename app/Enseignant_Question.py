import secrets

from __Module_utility_Function__ import _array_equal_elements

class Question:
    # Attributes Question
    Enonce = ""
    Q_responses = []
    Q_proposition = []
    E_thematique = []
    Id_question = None
    type_question = ""
    #########

    def __init__(
        self, enonce_saisie, array_proposition, reponse_saisie, thematique_saisie,type_question
    ):
        self.Enonce = enonce_saisie
        self.Q_responses = reponse_saisie
        self.Q_proposition = array_proposition
        self.E_thematique = thematique_saisie
        self.Id_question = secrets.token_hex(4)
        self.type_question = type_question
    # ! Verifie si les reponses saisie sont égales a ceux enregeistrer
    
    def response_is_correct(self,array_response_saisie):
        return _array_equal_elements(array_response_saisie,self.Q_responses)
    # def _response_percentage_correct(self,array_response_saisie):
    #     nbr_answer_correct = 0
    #     for answer in array_response_saisie:
    #         if (array_response_saisie[answer] in self.Q_responses):
    #             nbr_answer_correct += 1
    #     return (nbr_answer_correct // self.Q_responses) * 100 # renvoie le pourcentage de reponse correct
    def getJson_info(self):
        info_question_formated = {
            "id": self.Id_question,
            "enonce": self.Enonce,
            "propositions": self.Q_proposition,
            "reponses": self.Q_responses,
            "thematique": self.E_thematique,
            "type_question": self.type_question
        }
        return info_question_formated
    
    def setID_q(self,id_):
        self.Id_question = id_
    