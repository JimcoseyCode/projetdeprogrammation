import json
import os
from datetime import datetime



class Statistique: 

    user_name = None
    link_database_livestream = None

    def __init__(self, user_name):
        self.user_name = user_name
        self.link_database_livestream = (
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE"
        )

    def _give_students_Info(self):
        # ? specification ->
        """
        donner les infos de chaque etudiant ayant participer dans des lives d'un enseignant 
        """
        object_final = {}  # dict -> qui contient info students
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(self.link_database_livestream):
                with open(f"{self.link_database_livestream}/{file_name}") as file_:
                    data_file = json.load(file_)["espace_students"]
                    if (data_file != {}):
                        for student in data_file.values():
                            object_students = {
                                "info_student": {
                                    "numero_etudiant": student['info_student']['numero_etudiant'],
                                    "nom": student['info_student']['nom'],
                                    "prenom": student['info_student']['prenom'],
                                }
                            }
                            object_final[f"student_{student['info_student']['numero_etudiant']}"] = (
                                object_students)
        print(object_final)
        return object_final
    def response_good(self, num_etudiant):
            data_object = {}
            student = {
                f"student_{num_etudiant}": {
                    "info_student": {
                        "numero_etudiant": num_etudiant,
                        "nom": None,
                        "prenom": None,
                    },
                    "Live": {},
                },
            }

            if os.path.exists(self.link_database_livestream):
                for file_name in os.listdir(self.link_database_livestream):
                    # with open(f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json","r") as file:
                    question_ = {}
                    with open(f"{self.link_database_livestream}/{file_name}") as file:
                        data = json.load(file)
                        for etudiant in data["espace_students"].values():
                            if (etudiant["info_student"]["numero_etudiant"]) == num_etudiant:# cherche l'etudiant en question
                                student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"] = {}# me creer l'object live_idlive
                                student[f"student_{num_etudiant}"]["info_student"]["nom"] = etudiant["info_student"]["nom"]
                                student[f"student_{num_etudiant}"]["info_student"]["prenom"] = etudiant["info_student"]["prenom"]
                                student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"]["response_correct"]=None
                                # on calcul les reponses correctes en fonction du type du live correspondant
                
                                # on insere dans l'object (question_) tous les attribut des answer c'est a dire(id_question_answered, date ,answer)
        
                                for cle_answer in etudiant["answer_live"].values():
                                    question_[f"question_{cle_answer['id_question_answered']}"] = cle_answer  # cle_answer est stocke dans l'object question_                            
                                    student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"][f"question_{cle_answer['id_question_answered']}"]=cle_answer
                                    if  data["live_type"] == "QLive":
                                        student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"]["response_correct"] = self.response_good_by_students(data["id_live"], num_etudiant)
                                    elif data["live_type"] == "SeqLive":
                                        student[f"student_{num_etudiant}"]["Live"][f"live_{data['id_live'][-8:len(data['id_live'])]}"]["response_correct"] = self.response_correcte_pourcentage_seqlive(data["id_live"], num_etudiant)
                data_object=student
            return data_object
        
 # donne le nombre de reponse correcte d'un etudiant d'un qlive
    def response_good_by_students(self, id_live, num_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            s = 0
            for student in data["espace_students"].values():
                if student["info_student"]["numero_etudiant"] == num_etudiant:
                    # print(student["info_student"])
                    for cle_answer in student["answer_live"].values():
                        if cle_answer["answer"] == True:
                            s += 1
            return s

    def _give_stat_Student(self, numero_etudiant):  # students_xxxx
        # ? specifications 
        """
        donne les stat de chaque etudiant -> un object contenant 2 tableau
            l'un qui contient les info de type qlive
          et l'autre seqlive
        """
        p_live_content = {
            "q_live":[],
            "seq_live":[]
        } # Object contenenant tout les les infos des lives que l'etudiant a participé
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(self.link_database_livestream):
                with open(f"{self.link_database_livestream}/{file_name}") as file_:
                    data_file = json.load(file_)
                    if (data_file["live_type"] == "QLive" and f"student_{numero_etudiant}" in data_file["espace_students"]):
                        if (data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"] != {}):
                            for answer in data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"].values():
                                object_answer = {
                                    "id_live": data_file["id_live"],
                                    "id_question": answer["id_question_answered"] ,
                                    "answer": answer["answer"], 
                                }
                                p_live_content["q_live"].append(object_answer)

                    if (data_file["live_type"] == "SeqLive" and f"student_{numero_etudiant}" in data_file["espace_students"]):
                        if (data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"] != {}):
                            for answer in data_file["espace_students"][f"student_{numero_etudiant}"]["answer_live"].values():
                                object_answer = {
                                    "id_live": data_file["id_live"],
                                    "id_question": answer["id_question_answered"] ,
                                    "answer": self.response_correcte_pourcentage_seqlive(data_file["id_live"],numero_etudiant), 
                                }
                                p_live_content["seq_live"].append(object_answer)
            # supression doublons 
            ids_vus = set()
            uniques = []
            for dictionnaire in p_live_content["seq_live"] :
                if dictionnaire['id_live'] not in ids_vus:
                    ids_vus.add(dictionnaire['id_live'])
                    uniques.append(dictionnaire)
            p_live_content["seq_live"] = uniques
            # print(p_live_content["seq_live"])
            return p_live_content
                              
            

        #                         object_final[f"student_{student['info_student']['numero_etudiant']}"] = (
        #                             object_students)
        # return object_final
    def response_correcte_pourcentage_seqlive(self, id_live, numero_etudiant):
        with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
            data = json.load(file)
            response_correct = 0
            pourcentage = 0
            for student in data:
                if data["live_type"] == "SeqLive":
                    for students in data["espace_students"].values():
                        if (
                            students["info_student"]["numero_etudiant"]
                            == numero_etudiant
                        ):
                            for donnee_answer_live in students["answer_live"].values():
                                if donnee_answer_live["answer"] == True:
                                    response_correct += 1
                                    pourcentage = response_correct / len(
                                        students["answer_live"]
                                    )
                return str(round(pourcentage * 100,2)) + "%"  # retourne en pourcentage
            # la fonction round arrondi a l'entier superieur si c'est superieur a la moitie,sinon a l'entier inferieur

    def effacer_response(self, id_live):
        if os.path.exists(self.link_database_livestream):
            # for file_name in os.listdir(self.link_database_livestream):
                with open(
            f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
            "r",
        ) as file:
                    data = json.load(
                        file
                    )  # c'est memorise en memoire, retourne un objet json
                    for student in data["espace_students"]:
                        # print(student)
                        data["espace_students"][student]["answer_live"] = {}
                with open(
                    f"./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json",
                    "w",
                ) as file_response:
                    json.dump(data, file_response, indent=4)

    def response_seqlive_by_etudiant(self, id_live, user_name, numero_etudiant):
        self.inscrire_all_student_live_json(
            self.user_name, id_live, numero_etudiant)
        if os.path.exists(
            f"./components/LiveStream_database/all_student_connected_live.json"
        ):
            with open(
                "./components/LiveStream_database/all_student_connected_live.json", "w"
            ) as files:
                data_all_student_connected = json.load(files)
                for i in data_all_student_connected:
                    data_all_student_connected["numero_etudiant"] == numero_etudiant
                    with open(
                        f"./components/LiveStream_database/{data_all_student_connected[i][user_name]}_Data_LIVE/{id_live}_Data.json",
                        "r",
                    ) as file:
                        data_user_name = json.load(file)
                        s = 0
                        for i in data_user_name:
                            if data_user_name[i]["answer"] == "True":
                                s += 1
                            s = i / len(data_user_name)
                    return str(round(s * 100)) + "%"
                print(s)

    def data_participant(self):
        data = {}
        # url="./components/LiveStream_database/{self.user_name}_Data_LIVE/{id_live}_Data.json"
        # link_database_livestream="./components/LiveStream_database/raphael_Data_LIVE"
        if os.path.exists(self.link_database_livestream):
            for file_name in os.listdir(
                self.link_database_livestream
            ):  # listdir : donne un tableau ou il y aura les noms de chemin de chaque fichier

                data_object = {
                    "id_live": None,
                    "date_live_stream": None,
                    "number_etudiant": None,
                }

                with open(f"{self.link_database_livestream}/{file_name}") as file:
                    data_live = json.load(file)
                    data_object["id_live"] = data_live["id_live"]
                    data_live["date_live_stream"]
                    data_object["date_live_stream"] = str(
                        datetime.strptime(
                            data_live["date_live_stream"], "%Y-%m-%d %H:%M:%S.%f"
                        ).strftime("%-d %B %Y à %H:%M:%S")
                    )
                    data_object["number_etudiant"] = self.number_etudiant(
                        data_object["id_live"]
                    )
                    data[file_name] = data_object

        # ! Trie des données avant d'etre retourner
        def compare_dates(key_obj):
            return data[key_obj]["date_live_stream"]

        # Tri des clés de sous-objet par date
        key_sorted = sorted(data.keys(), key=compare_dates)
        # Création d'un nouveau dictionnaire trié à partir du dictionnaire original
        data_sorted = {cle: data[cle] for cle in key_sorted}
        return data_sorted

    def number_etudiant(self, id_live):
        if os.path.exists(self.link_database_livestream):  # c'est dynamique
            with open(
                f"{self.link_database_livestream}/{id_live}_Data.json", "r"
            ) as file:  # f permet de mettre des variables (ici qui sont de types string)
                data = json.load(file)
                s = 0
                for students in data["espace_students"]:
                    s += 1
                return s

b=Statistique("raphael")
b.effacer_response("SeqLive - 186797d0")