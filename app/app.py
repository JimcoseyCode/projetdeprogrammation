import json
import uuid
from flask import Flask, render_template, request, redirect
from flask_socketio import SocketIO, send, emit, join_room, leave_room
import os
from statistique import Statistique
# flask --app app --debug run

from GestionUser_Engine import GestionUser
app = Flask(__name__)
app.config['SECRET_KEY'] = 'UM'

socketio = SocketIO(app)
def get_app():
    return socketio


app.debug = True
# ! GESTIONAIRE D'UTILISATEUR -- >
gestion_user_Serveur = GestionUser()
# ! GESTION DES SOCKET -- >
# Creation d'une chambre par live


@socketio.on('create_room')
def create_room(data):
    room = data['room']
    join_room(room)  #avec sa on rejoind la chambre tout au debut
    print(
        f"Un live a été créée par {data['owner_room']} -> chambre [{data['room']}]")



@socketio.on('join_room_live')
def join_room_live(data):
    room = data['room']
    join_room(room)
    print(f'{data["etudiant"]} Vous venez de rejoindre la room {data["room"]}')


@app.route("/", methods=['GET', 'POST'])
def App():
    if request.method == 'POST':
        # Verifie si les identifiants sont correct
        if "login" in request.form and gestion_user_Serveur._login_User(request.form["user_name"], request.form["user_password"], request.form["type_user"]):
            if request.form["type_user"] == "enseignant":
                return redirect("/dashboard/enseignant/" + request.form["user_name"], code=302)
            if request.form["type_user"] == "etudiant":
                return redirect("/dashboard/etudiant/" + request.form["user_name"], code=302)
        if "login" in request.form and gestion_user_Serveur._login_User(request.form["user_name"], request.form["user_password"], request.form["type_user"]) == False:
            return render_template("app.html", error_connexion="Votre mot de passe ou nom d'utilisateur est incorrect !")
        if "register" in request.form:
            return redirect("/inscription", code=302)
    return render_template("app.html")


@app.route("/disconnect/<type_user>/<user_name>", methods=['GET', 'POST'])
def disconnect_user(user_name, type_user):
    gestion_user_Serveur._disconnected(type_user, user_name)
    return redirect("/", code=302)


@app.route("/inscription", methods=['GET', 'POST'])
def inscription():
    if request.method == "POST":
        gestion_user_Serveur.registration_Enseignant(
            request.form["user_name"], request.form["user_password"])
        return redirect("/", code=302)
    return render_template("/components/__create_account.html")


# ! [DATA :: (ROUTES_APP)]
@app.route("/database/question/enseignant/<user_name>", methods=['GET', 'POST'])
def dataBaseQuestion(user_name):
    data_etiquettes = gestion_user_Serveur.get_user(
        user_name, "enseignant").getInfo_user(user_name, "all_etiquettes")
    stat_thematique = gestion_user_Serveur.get_user(
        user_name, "enseignant")._get_thematique_stat()
    print("les stat")
    # print(stat_thematique)

    if request.method == "POST":

        return render_template("components/__interface_question.html", user_name=user_name, all_questions_user=gestion_user_Serveur.get_user(user_name, "enseignant").getInfo_user(user_name, "all_questions"), all_questions_user_thematique=data_etiquettes, stat_thematique=stat_thematique)
    return render_template("components/__interface_question.html", user_name=user_name, all_questions_user=gestion_user_Serveur.get_user(user_name, "enseignant").getInfo_user(user_name, "all_questions"), all_questions_user_thematique=data_etiquettes, stat_thematique=stat_thematique)


@app.route("/generate_controle/<user_name>", methods=['GET', 'POST'])
def generate_controle(user_name):

    if request.method == "POST":

        data_controle = gestion_user_Serveur.get_user(
            user_name, "enseignant"). _choise_typeG_controle(request.form["type_order"], int(request.form["nbr_controle"]), json.loads(request.form["data_controle"]),request.form["nbr_questions"])

        return render_template("/components/__generateControle.html", all_sujets=data_controle)

@app.route("/dashboard/enseignant/<user_name>")
def dashboard_enseignant(user_name):
    data_etiquettes = gestion_user_Serveur.get_user(
        user_name, "enseignant").getInfo_user(user_name, "all_etiquettes")
    return render_template("components/__dashboard_enseignant.html", user_name=user_name, all_questions_user_thematique=data_etiquettes)


# STUDENTS

@app.route("/dashboard/etudiant/<user_name>", methods=['GET', 'POST'])
def dashboard_etudiant(user_name):
    utilisateur_courant = gestion_user_Serveur.get_user(user_name, "etudiant")
    profile = utilisateur_courant.get_Profile(user_name)
    return render_template("components/__dashboard_student.html", nom=profile["nom"], prenom=profile["prenom"], numero_etudiant=profile["numero_etudiant"], user_name=user_name, age=profile["age"], nationalite=profile["nationalite"], parcours=profile["parcours_scolaire"], sexe=profile["sexe"], classe=profile["classe"], erreur_mdp="")


@app.route("/modifier_info_etudiant/<user_name>", methods=['GET', 'POST'])
def modifier_info_student(user_name):
    utilisateur_courant = gestion_user_Serveur.get_user(user_name, "etudiant")
    profile = utilisateur_courant.get_Profile(user_name)
    mdp_old = profile["mot_de_passe"]
    mdp_new = ""
    saisieErreur = ""

    if request.method == "POST":
        if request.form["ancien_mdp"] == mdp_old and request.form["mdp"] != "":
            saisieErreur = ""
            mdp_new = request.form["mdp"]
        elif request.form["ancien_mdp"] == "" and request.form["mdp"] == "":
            saisieErreur = ""
            mdp_new = mdp_old
        else:
            mdp_new = mdp_old
            saisieErreur = "mot de passe incorrect !"
        utilisateur_courant.modifier_Profile(user_name, request.form["age"], request.form["nationalite"],
                                             request.form["parcours"], request.form["sexe"], request.form["classe"], mdp_new, mdp_old)

    profile = utilisateur_courant.get_Profile(user_name)
    return render_template("components/__dashboard_student.html", nom=profile["nom"], prenom=profile["prenom"], numero_etudiant=profile["numero_etudiant"], user_name=user_name, age=profile["age"], nationalite=profile["nationalite"], parcours=profile["parcours_scolaire"], sexe=profile["sexe"], classe=profile["classe"], erreur_mdp=saisieErreur)


# ! [FUNCTIONALITY :: (ROUTES)]

@app.route("/create_question/enseignant/<user_name>", methods=['GET', 'POST'])
def create_question(user_name):
    json_str = request.form.get("question_object")
    data = json.loads(json_str)
    gestion_user_Serveur.get_user(user_name, "enseignant").addQuestion(
        data['enonce'], data['propositions'], data['reponses'], data['thematique'], data["type_question"])
    return redirect("/dashboard/enseignant/" + user_name, code=302)


@app.route("/remove_question/<user_name>", methods=['GET', 'POST'])
def suprimer_question(user_name):
    if request.method == "POST":
        gestion_user_Serveur.get_user(user_name, "enseignant").suprimerQuestion(
            request.form["id_question"])
        return redirect("/database/question/enseignant/" + user_name, code=302)


@app.route("/modify_question/<id_u_question>/<user_name>", methods=['GET', 'POST'])
def modify_question(user_name, id_u_question):
    if request.method == "POST":
        if "id_question" in request.form:
            current_question = gestion_user_Serveur.get_user(
                user_name, "enseignant").getSpecifiqueQuestion(request.form["id_question"])
            return render_template("components/__modif_question.html", user_name=user_name, question_object_before=current_question)
        if "question_object" in request.form:
            json_str = request.form["question_object"]
            print(json_str)
            data = json.loads(json_str)
            current_question = gestion_user_Serveur.get_user(
                user_name, "enseignant").getSpecifiqueQuestion(id_u_question)
            indice_question = gestion_user_Serveur.get_user(
                user_name, "enseignant")._getIndice_question(id_u_question)
            gestion_user_Serveur.get_user(
                user_name, "enseignant").suprimerQuestion(id_u_question)
            gestion_user_Serveur.get_user(user_name, "enseignant").modify_question(
                data['enonce'], data['propositions'], data['reponses'], data['thematique'], indice_question, data["type_question"])
            return redirect("/database/question/enseignant/" + user_name, code=302)


@app.route("/Live/<user_name>/settings", methods=['GET', 'POST'])
def live_stream_settings(user_name):
    data_etiquettes = gestion_user_Serveur.get_user(
        user_name, "enseignant").getInfo_user(user_name, "all_etiquettes")
    if request.method == "POST":
        if "settings_data_live" in request.form:

            # objet envoyer par le client
            data_live = json.loads(request.form.get("settings_data_live"))
            # print(data_live) #ici
            gestion_user_Serveur.get_user(user_name, "enseignant")._create_live_stream(
                data_live["live_type"], data_live["Q_id_selected"])
            current_live = gestion_user_Serveur.get_user(
                user_name, "enseignant")._current_LIVE_STREAM
            try:
                if (current_live.type_live == "QLive"):
                    temp_id = current_live.id_uLive[-8:len(
                        current_live.id_uLive)]
                    # print("1  : " + current_live.id_question_live)
                    print(f"qLive - {temp_id}")
                    # socketio.emit("create_room",{"room":current_live.id_uLive,"owner_room": user_name })

                    # print(f"qLive - {temp_id}" == current_live.id_question_live)
                    # print("/Live/"+user_name+"/qLive/"+temp_id)
                    return redirect("/Live/"+user_name+"/qLive/"+temp_id)
            except:
                return redirect(f"/Live/{user_name}/settings")
            if (current_live.type_live == "SeqLive"):
                temp_id = current_live.id_sequence_live[-8:len(
                    current_live.id_sequence_live)]
                return redirect("/Live/"+user_name+"/SeqLive/"+temp_id)
    return render_template("./components/LiveStream/__LiveStream.html", user_name=user_name, all_questions_user=gestion_user_Serveur.get_user(user_name, "enseignant").getInfo_user(user_name, "all_questions"), active2="active", active="", show="show", show2="", disable_settings="grid", all_questions_user_thematique=data_etiquettes)


@socketio.on('next_question')
def next_question(data):
    instance_live = gestion_user_Serveur._getLive_instance(data["id_live"])
    print('nouvelle question: ' + str(data))
    # Passer a la question suivante
    instance_live._nextQuestion()
    # emit('enonce', {'message': ''},room = )
    # socketio.emit('enonce', {'message': ''}, broadcast=True)
    socketio.emit('enonce', {'message': ''}, room=data["id_live"])

    print(
        f"/Live/{instance_live.owner_live}/{instance_live.type_live}/{data['id_live'][-8:len(data['id_live'])]}")
    # return redirect(f"/Live/{instance_live.owner_live}/{instance_live.type_live}/{data['id_live']}")


@app.route("/Live/<user_name>/<type_live>/<id_live>", methods=['GET', 'POST'])
def live_stream(user_name, id_live, type_live):
    # print("live stream : ")
    # print(f"{type_live} - {id_live}")
    # try:
    # print(gestion_user_Serveur.Verif_code_acces_Live(f"{type_live} - {id_live}"))
    if (gestion_user_Serveur.Verif_code_acces_Live(f"{type_live} - {id_live}") == True):
        current_live = gestion_user_Serveur.get_user(
            user_name, "enseignant")._current_LIVE_STREAM
        print(
            f"Voila le nombre de gens connectée sur le live {current_live.id_uLive} : {current_live.number_participant()}")

        if type_live == "qLive":
            # Avec les chambres

            socketio.emit('stat_user_answered', {
                'nbr_user_answered': current_live.number_answered(),
                'id_live': current_live.id_uLive
            },to = current_live.id_uLive)

            socketio.emit('stat_user_connected', {
                'nbr_user_connected': current_live.number_participant(),
                'id_live': current_live.id_uLive
            },to = current_live.id_uLive)
            socketio.emit('giveData_answer_Livestream',
                          current_live._getStat_answer(),to = current_live.id_uLive)

            return render_template("./components/LiveStream/LiveStream_dashboard.html", user_name=user_name, current_live=current_live)
        elif type_live == "SeqLive":
            print("vpoici les")
            socketio.emit('stat_user_answered', {
                'nbr_user_answered': current_live.number_answered(),
                'id_live': current_live.id_uLive
            },to = current_live.id_uLive)
            socketio.emit('stat_user_connected', {
                'nbr_user_connected': current_live.number_participant(),
                'id_live': current_live.id_uLive
            },to = current_live.id_uLive)
            socketio.emit('giveData_answer_Livestream',
                          current_live._getStat_answer(),to = current_live.id_uLive)


            return render_template("./components/LiveStream/_LiveStream_sequence.html", user_name=user_name, current_live=current_live)
    # except:
    return "Desoler aucun live sur cette id n'est encours"


@app.route("/Live/<user_name>/stop", methods=['GET', 'POST'])
def stop_live_stream(user_name):
    current_live = gestion_user_Serveur.get_user(
        user_name, "enseignant")._current_LIVE_STREAM
    if request.method == "POST":
        if "type_live" in request.form:
            socketio.emit('stop_live_client',to = current_live.id_uLive)
            gestion_user_Serveur.get_user(
                user_name, "enseignant")._stop_live_stream(request.form["type_live"])
            print(current_live)
            return redirect(f"/dashboard/enseignant/{user_name}")


@app.route("/change-password/<user_name_enseignant>", methods=['GET', 'POST'])
def change_password(user_name_enseignant):
    msg_change_pwd = {
        "isOk": f'[] Vous venez de modifier votre mot de passe !',
        "isNotOk": "Un probleme est survenue lors du changement de votre mot de passe : [older_password_saisie] est incorrect !!!",
    }
    current_user = gestion_user_Serveur.get_user(
        user_name_enseignant, "enseignant")
    if request.method == "POST":
        if current_user._change_password(request.form["older_password"], request.form["new_password"]):
            # saisieErreur = msg_change_pwd["isOk"]
            return redirect("/dashboard/enseignant/" + user_name_enseignant, code=302)
        else:
            # saisieErreur = msg_change_pwd["isNotOk"]
            return redirect("/dashboard/enseignant/" + user_name_enseignant, code=302)


@app.route("/create_account/<user_name>", methods=['GET', 'POST'])
def create_account(user_name):
    if request.method == "POST":
        file_saisie = request.files["file"]
        if file_saisie:
            file_saisie.save(os.path.join(
                "./components/user_info", "Students.csv"))
            gestion_user_Serveur.get_user(user_name, "enseignant").create_student_account(
                "./components/user_info/Students.csv")
            return redirect("/dashboard/enseignant/" + user_name, code=302)


@app.route("/generate-page/<user_name>", methods=['GET', 'POST'])
def generate_page(user_name):
    json_str = request.form.get("generate_page__object")
    data = json.loads(json_str)
    if request.method == "POST":
        print(data["Q_id_selected"])
        current_user = gestion_user_Serveur.get_user(user_name, "enseignant")
        return render_template("/components/__generatedPage.html", all_questions_user=current_user.getQuestions_(data["Q_id_selected"]), title_page=data["title"])


@socketio.on('search_by_theme')
def search_question_by_theme(data_obj):
    current_user = gestion_user_Serveur.get_user(
        data_obj["user_name"], "enseignant")
    search_items = data_obj["search_items"]
    # data = json.loads(json_str)
    object_questions = current_user.getQuestion_by_theme(search_items)
    id_all_question_found = []
    for question in object_questions:
        id_all_question_found.append(question["id"])
    # data_etiquettes = current_user.getInfo_user(user_name, "all_etiquettes")
    # if (id_all_question_found == []):
    #     socketio.emit("search_by_theme","Il n'ya aucune question de type " + str(res))
    print(current_user.getQuestions_(id_all_question_found))
    socketio.emit("search_by_theme",
                  current_user.getQuestions_(id_all_question_found))

# LIVE :


def _LiveConnect(id_Live, user_nameEtudiant):

    target_live = gestion_user_Serveur._getLive_instance(id_Live)
    i_etudiant = gestion_user_Serveur.get_user(
        user_nameEtudiant, "etudiant")
    target_live._connect(i_etudiant)

    socketio.emit('stat_user_connected', {
        'nbr_user_connected': target_live.number_participant(),
        'id_live': target_live.id_uLive
    },to = target_live.id_uLive)
    print(
        f"Voila le nombre de gens connectée sur le live {target_live.id_uLive} : {target_live.number_participant()}")


# ! LIVESTREAM --> SOCKET


@socketio.on('giveData_answer_Livestream')
def giveData_answer_Livestream(id_live):
    live_instance = gestion_user_Serveur._getLive_instance(id_live)
    socketio.emit('giveData_answer_Livestream',
                live_instance._getStat_answer(),room = live_instance.id_uLive)

 # ! Live en cours optimizée

 


@app.route("/Live/<user_name>", methods=['GET', 'POST'])
def live_en_cours(user_name):

    if request.method == "POST":
        _LiveConnect(request.form["code_acces"], request.form["user_name"])
        live_instance_ = gestion_user_Serveur._getLive_instance(
                            request.form["code_acces"])

        if "reponses" in request.form:
            code_acces = request.form["code_acces"]
            if gestion_user_Serveur.Verif_code_acces_Live(code_acces) == True:
                objet_type = gestion_user_Serveur.get_Type_LIve(code_acces)

                if objet_type == "QLive":
                    # Info utiles
                    objet_question_live = gestion_user_Serveur.get_Info_Live_objet(
                        code_acces)
                    question_obj = gestion_user_Serveur._getLive_instance(
                        request.form["code_acces"]).question_live_content
                    type_question = json.dumps(
                        objet_question_live["type_question"])
                    enonce = json.dumps(objet_question_live["enonce"])
                    tableau_propositions = json.dumps(
                        objet_question_live["propositions"])

                    obj_q_utile = {
                        "enonce": question_obj.Enonce,
                        "propositions": question_obj.Q_proposition,
                        "reponses": question_obj.Q_responses,
                        "id_question": question_obj.Id_question
                    }
                    if request.form["reponses"] != []:
                        # ça marche !
                        print("je suis ici !!!!!!!!!!!!!!!!!!!!! recu")
                        print(request.form["reponses"] )
                        print(json.loads(request.form["reponses"]))
                        gestion_user_Serveur.Ajout_proposition_Live_QO(request.form["code_acces"], json.loads(request.form["reponses"]))

                        # print(json.loads(request.form["reponses"]))
                        # print(request.form["reponses"])


                        gestion_user_Serveur.Ajouter_reponse_objet(code_acces, user_name, json.loads(request.form["reponses"]))
                        

                        # Avec chambre
                        socketio.emit('stat_user_answered', {
                            'nbr_user_answered': live_instance_.number_answered(),
                            'id_live': live_instance_.id_uLive
                        },to = live_instance_.id_uLive)
                        socketio.emit('giveData_answer_Livestream',
                                      live_instance_._getStat_answer(),to = live_instance_.id_uLive)

                        # print(code_acces + str(" je suis ici "))
                        return render_template("/components/Live.html", code_acces=code_acces, user_name=user_name, enonce=enonce, tableau_propositions=tableau_propositions, type_question=type_question, question_obj=obj_q_utile)

                    return render_template("/components/Live.html", code_acces=code_acces, user_name=user_name, enonce=enonce, tableau_propositions=tableau_propositions, type_question=type_question, question_obj=obj_q_utile)
                if objet_type == "SeqLive":
                    print("MMMMMM -> ")
                    print(gestion_user_Serveur._getLive_instance(code_acces)._current_questionLive)
                    if gestion_user_Serveur._getLive_instance(code_acces)._current_questionLive != None:
                        objet_question_seqLive = gestion_user_Serveur.get_Info_Live_objet_seqLive(
                            code_acces)

                        question_obj = gestion_user_Serveur._getLive_instance(
                            request.form["code_acces"])._current_questionLive
                        obj_q_utile = {
                            "enonce": question_obj.Enonce,
                            "propositions": question_obj.Q_proposition,
                            "reponses": question_obj.Q_responses,
                            "id_question": question_obj.Id_question
                        }
                        type_question = json.dumps(
                            objet_question_seqLive["type_question"])
                        enonce = json.dumps(objet_question_seqLive["enonce"])
                        tableau_propositions = json.dumps(
                            objet_question_seqLive["propositions"])
                        
                        print("Mes reponses : -> ")
                        print(request.form["reponses"])
                        print("--------------")

                        if "reponses" in request.form and json.loads(request.form["reponses"]) != []:
                                # print(request.form["reponses"])
                                if live_instance_._current_questionLive.type_question == "QO":
                                    gestion_user_Serveur.Ajout_proposition_seqLive_QO(
                                        request.form["code_acces"], json.loads(request.form["reponses"]))
                                    # # ! mettre a jour les reponses stat
                                    gestion_user_Serveur.Ajouter_reponse_objet(
                                        code_acces, user_name, json.loads(request.form["reponses"]))

                                elif (live_instance_._is_include(json.loads(request.form["reponses"]))):
                                    gestion_user_Serveur.Ajout_proposition_seqLive_QO(
                                        request.form["code_acces"], json.loads(request.form["reponses"]))
                                    # # ! mettre a jour les reponses stat
                                    gestion_user_Serveur.Ajouter_reponse_objet(
                                        code_acces, user_name, json.loads(request.form["reponses"]))
                            
                                socketio.emit('stat_user_answered', {
                                    'nbr_user_answered': live_instance_.number_answered(),
                                    'id_live': live_instance_.id_uLive
                                },to = live_instance_.id_uLive)
                                socketio.emit('giveData_answer_Livestream',
                                            live_instance_._getStat_answer(),to = live_instance_.id_uLive)
                        return render_template("/components/Live.html", code_acces=code_acces, user_name=user_name, enonce=enonce, tableau_propositions=tableau_propositions, type_question=type_question, question_obj=obj_q_utile)
                    else:
                        question_obj = gestion_user_Serveur._getLive_instance(
                            request.form["code_acces"])._current_questionLive
                        obj_q_utile = {
                            "enonce": "",
                            "propositions": "[]",
                            "reponses": "[]",
                            "id_question": ""
                        }

                        enonce = json.dumps("Live Terminer !")
                        tableau_propositions = json.dumps("[]")
                        type_question = json.dumps("terminer")
                        return render_template("/components/Live.html", code_acces=code_acces, user_name=user_name, enonce=enonce, tableau_propositions=tableau_propositions, type_question=type_question, question_obj=obj_q_utile)
            else:
                return redirect("/dashboard/etudiant/" + user_name)
    return redirect("/dashboard/etudiant/" + user_name)


@socketio.on('message_instantaner')
def envoie_tab_proposition(data):
    print(data["msg"])
    objet_type = gestion_user_Serveur.get_Type_LIve(data["_code_acces"])
    if objet_type == "QLive":
        proposition_en_cours = gestion_user_Serveur.get_InFo_Live_propositions(
            data["_code_acces"])
    else:
        proposition_en_cours = gestion_user_Serveur.get_InFo_Live_propositions_seqLive(
            data["_code_acces"])

    socketio.emit('proposition_instantaner', {'proposition_': proposition_en_cours},to=data["_code_acces"])



@app.route("/quitter_live/etudiant/<user_name>", methods=['GET', 'POST'])
def quitter_live(user_name):
    if request.method == "POST":
        try:
            print("Désolé, ce Live n'existe plus.")
            print(gestion_user_Serveur._getLive_instance(
                request.form["code_acces"]))
            gestion_user_Serveur._getLive_instance(
                request.form["code_acces"])._quit_Live(user_name)
            print("c'est fait !")
            current_live = gestion_user_Serveur._getLive_instance(
                request.form["code_acces"])
            socketio.emit('stat_user_connected', {
                'nbr_user_connected': current_live.number_participant(),
                'id_live': current_live.id_uLive
            },to = current_live.id_uLive)
            return redirect("/dashboard/etudiant/" + user_name)
        except AttributeError:
            return redirect("/dashboard/etudiant/" + user_name)
    return redirect("/dashboard/etudiant/" + user_name)


@socketio.on('print_correction_answer')
def print_correction(data):
    socketio.emit("print_correction_answer", {
                  'state_correction': data["state_correction"]},to = data["id_live"])


@socketio.on('stop_answer')
def stop_answer(id_Live):
    print("voici l'id de fermeyture " + id_Live)
    current_live = gestion_user_Serveur._getLive_instance(id_Live)
    if not current_live:
        return "Invalid id_live", 400
    current_live._stop_answer()
    redirect_url = f"/Live/{current_live.owner_live}/{current_live.type_live}/{id_Live[-8:]}"
    if current_live.type_live == "QLive":
        redirect_url = f"/Live/{current_live.owner_live}/qLive/{id_Live[-8:]}"
    elif current_live.type_live == "SeqLive":
        redirect_url = f"/Live/{current_live.owner_live}/SeqLive/{id_Live[-8:]}"
    return redirect(redirect_url, code=302)


# STATISTIQUES


@app.route("/Dashboard/statistique/<user_name>", methods=['GET', 'POST'])
def statistique(user_name):
    stat_enseignant = Statistique(user_name)

    if request.method == "POST":
        socketio.emit('data_diagramme', stat_enseignant.data_participant())
        return render_template("/components/diagramme.html", user_name=user_name, st_info=stat_enseignant._give_students_Info())
    return render_template("/components/diagramme.html", user_name=user_name, st_info=stat_enseignant._give_students_Info())


@socketio.on('giveData_student')
def data_student(data):
    stat_student = Statistique(data["owner_stat"])
    socketio.emit("giveData_student", stat_student._give_stat_Student(
        data["numero_etudiant"]))


@socketio.on('giveData_graph')
def give_DataGraph(data):
    stat_enseignant_graph = Statistique(data)
    # stat_enseignant_effacer_question=Statistique(data)
    print(data)
    socketio.emit('giveData_', stat_enseignant_graph.data_participant())


@socketio.on('giveData_effacer')
def effacer(data):
    stat_effacer = Statistique(data["owner_stat"])
    socketio.emit("giveData_effacer",
                  stat_effacer.effacer_response(data["id_live"]))


@app.route("/readme", methods=['GET', 'POST'])
def read_me():
    return render_template("/README.md")
