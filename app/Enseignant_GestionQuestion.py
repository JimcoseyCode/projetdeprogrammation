import json
import secrets
import random

from Enseignant_Question import Question
from __Module_utility_Function__ import (
    arrays_have_common_elements,
    add_object_to_json,
    modif_question_to_json,
)


class GestionQuestion:
    user_name = None
    file_questionLink = ""

    def __init__(self, u_name):
        self.user_name = u_name
        self.file_questionLink = (
            f"./components/user_info/Question_dataBase/{u_name}_question.json"
        )
        self.question_stock = self.getQuestion_by_theme(["all"])

    def getQuestion_by_theme(self, searched_thematique):
        array_result = []
        with open(self.file_questionLink) as file_:
            # ? Stock file(type(JSON)) => dictionaire
            stock_dic_JSON = json.load(file_)
            # Avoir tous les questions
            if searched_thematique[0] == "all":
                for key in stock_dic_JSON:
                    array_result.append(stock_dic_JSON[key])
                return array_result
            else:
                # Recherche par thématique en commun
                for key in stock_dic_JSON:
                    if (
                        arrays_have_common_elements(
                            searched_thematique, stock_dic_JSON[key]["thematique"]
                        )
                        == True
                    ):
                        array_result.append(stock_dic_JSON[key])
                return array_result

    def generatePage_questions(self, searched_thematique, titre_page_question):
        page_formated_JSON = {
            "title": titre_page_question,
            "questions": self.getQuestion_by_theme(searched_thematique),
        }
        # Création du fichier JSON vide
        with open(
            f"components/user_info/Page_dataBase/{self.user_name}_{titre_page_question}.json",
            "w",
        ) as f:
            json.dump({}, f)
        # Chargement du contenu du fichier JSON en mémoire
        with open(
            f"components/user_info/Page_dataBase/{self.user_name}_{titre_page_question}.json",
            "r",
        ) as file_:
            data = json.load(file_)
        # Ajout de l'objet "employee"
        data[titre_page_question] = page_formated_JSON
        # Ecriture du contenu modifié dans le fichier
        with open(
            f"components/user_info/Page_dataBase/{self.user_name}_{titre_page_question}.json",
            "w",
        ) as f:
            json.dump(data, f, indent=4)

    def _getIndice_question(self, id_question):
        with open(
            f"components/user_info/Question_dataBase/{self.user_name}_question.json",
            "r",
        ) as json_file:
            data = json.load(json_file)
            for key in data:
                if data[key]["id"] == id_question:
                    return data[key]["indice_question"]

    def addQuestion(self, enonce, array_proposition, array_response, array_etiquete, type_question):
        # Gere les etiquettes
        with open(
            f"./components/user_info/Etiquete/{self.user_name}_etiquette.json"
        ) as file_:
            # ? Stock file(type(JSON)) => dictionaire
            stock_dic_JSON = json.load(file_)
            array_etiquette_server = []
            for key in stock_dic_JSON:
                array_etiquette_server.append(
                    stock_dic_JSON[key]["thematique"])
            for i in range(len(array_etiquete)):
                if array_etiquete[i] not in array_etiquette_server:
                    etiquette_formated = {"thematique": ""}
                    etiquette_formated["thematique"] = array_etiquete[i]
                    add_object_to_json(
                        etiquette_formated,
                        "etiquette",
                        f"./components/user_info/Etiquete/{self.user_name}_etiquette.json",
                    )
        # Ajout de la question
        current_question = Question(
            enonce, array_proposition, array_response, array_etiquete, type_question
        )
        add_object_to_json(
            current_question.getJson_info(),
            "question" + secrets.token_hex(2),
            self.file_questionLink,
        )

    def suprimerQuestion(self, id_question):
        with open(self.file_questionLink, "r") as file_:
            buffer_read = json.load(file_)
            for key in buffer_read:
                if buffer_read[key]["id"] == id_question:
                    with open(self.file_questionLink, "r") as f:
                        data = json.load(f)
                    del data[key]
                    with open(self.file_questionLink, "w") as f:
                        json.dump(data, f, indent=4)
        self.update_indice()

    def update_indice(self):
        with open(self.file_questionLink, "r") as file_:
            buffer_read = json.load(file_)
            i = 1
            for key in buffer_read:
                buffer_read[key]["indice_question"] = str(i)
                i += 1
            with open(self.file_questionLink, "w") as f:
                json.dump(buffer_read, f, indent=4)

    def modify_question(
        self, enonce, array_proposition, array_response, array_etiquete, indice_, type_question
    ):
        # Gere les etiquettes
        with open(
            f"./components/user_info/Etiquete/{self.user_name}_etiquette.json"
        ) as file_:
            # ? Stock file(type(JSON)) => dictionaire
            stock_dic_JSON = json.load(file_)
            array_etiquette_server = []
            for key in stock_dic_JSON:
                array_etiquette_server.append(
                    stock_dic_JSON[key]["thematique"])
            for i in range(len(array_etiquete)):
                if array_etiquete[i] not in array_etiquette_server:
                    etiquette_formated = {"thematique": ""}
                    etiquette_formated["thematique"] = array_etiquete[i]
                    add_object_to_json(
                        etiquette_formated,
                        "etiquette",
                        f"./components/user_info/Etiquete/{self.user_name}_etiquette.json",
                    )
        # Ajout de la question
        current_question = Question(
            enonce, array_proposition, array_response, array_etiquete, type_question
        )
        modif_question_to_json(
            current_question.getJson_info(), "question", indice_, self.file_questionLink
        )

    def getSpecifiqueQuestion(self, id_question):
        with open(
            f"components/user_info/Question_dataBase/{self.user_name}_question.json",
            "r",
        ) as json_file:
            data = json.load(json_file)
            for key in data:
                if data[key]["id"] == id_question:
                    o_Json_question = data[key]
                    q_instance = Question(
                        o_Json_question["enonce"],
                        o_Json_question["propositions"],
                        o_Json_question["reponses"],
                        o_Json_question["thematique"],
                        o_Json_question["type_question"])
                    q_instance.setID_q(o_Json_question["id"])
                    return q_instance

    def getQuestions_(self, id_question_array):
        result_ = []
        with open(
            f"components/user_info/Question_dataBase/{self.user_name}_question.json",
            "r",
        ) as json_file:
            data = json.load(json_file)
            for id in id_question_array:
                for key in data:
                    if data[key]["id"] == id:
                        result_.append(data[key])
        return result_
    # Generation de pages

    def mix_array(self, ens_array):
        array_ = ens_array
        for i in range(len(array_)-1, 0, -1):
            j = random.randint(0, i)
            array_[i], array_[j] = array_[j], array_[i]
        return array_

    def pick_aleatoire_questions(self, array_, n):
        tableau = array_
        indices = list(range(len(tableau)))
        random_indices = random.sample(indices, n)
        elements = [tableau[i] for i in random_indices]
        return elements

    def generate_sujet(self, id_sujet, ens_sujets,type_s):
        # L'ensemble des questions qui seront recuperer
        obj_ = {
            "id_sujet": id_sujet,
            "questions": {

            }
        }
        for thematique in ens_sujets:
            # recupere un tableau de question
            # print(f"L'ensemble des questions [{thematique}] : {q_}")
            # print(q_)
            q_ = self.getQuestion_by_theme([thematique])
            print(q_)
            nbr_q = ens_sujets[thematique]


            if(type_s == "mix_order"):
                q_mixed = self.mix_array(q_)
                # les melanges les questions et prend un nombre de questions par intervales
                obj_["questions"][thematique] = self.pick_aleatoire_questions(q_mixed,nbr_q) 
            if(type_s == "ordered"):
                q_ordered = q_[::-1]
                # les melanges les questions et prend un nombre de questions par intervales
                obj_["questions"][thematique] = self.pick_aleatoire_questions(q_ordered,nbr_q) 

        if(type_s == "mix_order"):
            # self.mix_array(obj_["questions"])
            shuffled_keys = list(obj_["questions"].keys())
            # shuffled_keys = list(original_obj.keys())
            random.shuffle(shuffled_keys)
            shuffled_values = list(obj_["questions"].values())
            random.shuffle(shuffled_values)
            shuffled_obj = {}
            for i in range(len(shuffled_keys)):
                shuffled_obj[shuffled_keys[i]] = shuffled_values[i]
            obj_["questions"] = shuffled_obj


        return obj_

    def generer_valeurs_aleatoires_w(self,dictionnaire):
        nouveau_dictionnaire = {}
        for cle, valeur in dictionnaire.items():
            nouveau_dictionnaire[cle] = random.randint(
                valeur['min'], valeur['max'])
        return nouveau_dictionnaire
    
    
    def generer_valeurs_aleatoires(self,obj_,nbr_questions):
        """
        Specifications :
        Des intervales pour chaque thematique adpate les thématiques en fonction des min
        et max dans le quels si possible la l'intervale est respectée mais la sommes 
        de chaque valeur est égales au nombres de questions 
        """
        # result object_ du meme type de que celui en entrée 
        result_obj_ = {}
        for thematique_ in obj_:
            # genere un nombre par rapport a l'intervale 
            random_val_T = random.randint(obj_[thematique_]['min'], obj_[thematique_]['max'])
            # Ajout dans l'object final
            result_obj_[thematique_] = random_val_T
        # Additionner tout les random de l'object
        sum_total_T = sum(result_obj_.values())
        factor_mult = (nbr_questions / sum_total_T)
        for thematique_ in result_obj_:
            result_obj_[thematique_] = int(result_obj_[thematique_] * factor_mult)
        sum_total_T = sum(result_obj_.values())
        if sum_total_T != nbr_questions:
            # Thématique a ajuster pour avoir exactement nbr_questions apres avoir random et adapeter les nombres 
            thematique_ajust = list(result_obj_.keys())[0]
            result_obj_[thematique_ajust] += nbr_questions - sum_total_T
        print(result_obj_)
        return result_obj_


        




    def _choise_typeG_controle(self,type_,nbr_sujets,ens_thematiques,nbr_questions):
        """
        :: -> en fonction de la thématique il genere des sujets 
        
        """
        # L'ensemble des sujets final
        ens_sujets_final = {

        }
        # ? Dictionnaire avec les thematiques -> min et max
        # if (nbr_questions)
        if (nbr_questions == ""):
            # obj_s = self.generer_valeurs_aleatoires(ens_thematiques)
            if (type_ == "ordered"):
                for _ in range(1, nbr_sujets+1):
                    """
                    Spécification : {
                        thematique1 : {
                            min: 2,
                            max : 5
                        }
                        thematique2 : {
                            min: 2,
                            max : 10
                        }
                        thematique3 : {
                            min: 2,
                            max : 5
                        }
                    }
                    """
                    obj_s = self.generer_valeurs_aleatoires_w(ens_thematiques)
                    # Generation de l'id du sujets en cours
                    id_sujet = secrets.token_hex(2)
                    ens_sujets_final[f"s_{id_sujet}"] = self.generate_sujet(id_sujet, obj_s,"ordered")

            if (type_ == "mix_order"):
                for _ in range(1, nbr_sujets+1):
                    # Generation de l'id du sujets en cours
                    obj_s = self.generer_valeurs_aleatoires_w(ens_thematiques,)
                    id_sujet = secrets.token_hex(2)
                    ens_sujets_final[f"s_{id_sujet}"] = self.generate_sujet(id_sujet, obj_s,"mix_order")
            # obj_s = self.generer_valeurs_aleatoires(ens_thematiques)
        else:
            n_ = int(nbr_questions)
            if (type_ == "ordered"):
                for _ in range(1, nbr_sujets+1):
                    """
                    Spécification : {
                        thematique1 : {
                            min: 2,
                            max : 5
                        }
                        thematique2 : {
                            min: 2,
                            max : 10
                        }
                        thematique3 : {
                            min: 2,
                            max : 5
                        }
                    }
                    """
                    obj_s = self.generer_valeurs_aleatoires(ens_thematiques,n_)
                    # Generation de l'id du sujets en cours
                    id_sujet = secrets.token_hex(2)
                    ens_sujets_final[f"s_{id_sujet}"] = self.generate_sujet(id_sujet, obj_s,"ordered")

            if (type_ == "mix_order"):
                for _ in range(1, nbr_sujets+1):
                    # Generation de l'id du sujets en cours
                    obj_s = self.generer_valeurs_aleatoires(ens_thematiques,n_)
                    id_sujet = secrets.token_hex(2)
                    ens_sujets_final[f"s_{id_sujet}"] = self.generate_sujet(id_sujet, obj_s,"mix_order")
        print(f"L'ensemble final : {ens_sujets_final}")
        return ens_sujets_final

                 



    # def _generation_controles(self, nbr_sujets, _thematiques):
    #     """
    #     f : -> genere des controles
    #     input(nbr_sujets ,_thematiques) => 
    #     sujet : {
    #         thematique : {
    #             min: 2,
    #             max : 5
    #         }
    #         thematique : {
    #             min: 2,
    #             max : 10
    #         }
    #         thematique : {
    #             min: 2,
    #             max : 5
    #         }
    #     }

    #     """

    #     sujets_result = {

    #     }
    #     # un object pour
    #     obj_s = self.generer_valeurs_aleatoires(_thematiques)
    #     for _ in range(1, nbr_sujets+1):
    #         id_sujet = secrets.token_hex(2)
    #         sujets_result[f"s_{id_sujet}"] = self.generate_sujet(
    #             id_sujet, obj_s)

    #     return sujets_result
