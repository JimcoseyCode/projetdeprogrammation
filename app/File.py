import random

# def generer_sujets(etiquettes, encadrements, nb_questions, nb_sujets):
#     sujets = set()
#     while len(sujets) < nb_sujets:
#         sujet = []
#         for i in range(len(etiquettes)):
#             nb_q = random.randint(encadrements[i][0], encadrements[i][1])
#             sujet.extend([etiquettes[i]] * nb_q)
#         random.shuffle(sujet)
#         sujets.add(tuple(sujet))
#     return list(sujets)


# etiquettes = ['Java', 'compilation', 'PHP']
# encadrements = [(3, 5), (3, 4), (2, 4)]
# nb_questions = 20
# nb_sujets = 10

# sujets = generer_sujets(etiquettes, encadrements, nb_questions, nb_sujets)
# for i, sujet in enumerate(sujets):
#     print(f"Sujet {i+1}: {sujet}")

# obj_ = {
#     "math": {
#         "min": 2,
#         "max": 5
#     },
#     "histoire": {
#         "min": 4,
#         "max": 6
#     },"informatique": {
#         "min": 2,
#         "max": 8,
#     }
#     }


# def convert_object(obj, n):
#     sum_range = sum(obj[key][0] for key in obj)
#     factor = n / sum_range
#     new_obj = {}

#     sum_values = 0
#     for key in obj:
#         range_value = obj[key][0]
#         new_range = round(range_value * factor)
#         new_min = sum_values
#         new_max = new_min + new_range
#         sum_values += new_range
#         new_obj[key] = {'min': new_min, 'max': new_max}

#     return new_obj



# print(convert_object(obj_,20))




import random

import random

# def generate_values(obj, n):
#     new_obj = {}
#     for key in obj:
#         min_val = obj[key]["min"]
#         max_val = obj[key]["max"]
#         new_obj[key] = random.randint(min_val, max_val)
#     total_sum = sum(new_obj.values())
#     factor = n / total_sum
#     for key in new_obj:
#         new_obj[key] = int(new_obj[key] * factor)
#     sum_new_obj = sum(new_obj.values())
#     if sum_new_obj != n:
#         diff = n - sum_new_obj
#         # ajuster une valeur pour correspondre à la différence
#         key_to_adjust = list(new_obj.keys())[0]
#         new_obj[key_to_adjust] += diff
#     return new_obj


import random

def generate_values(obj, n):
    new_obj = {}
    for key in obj:
        min_val = obj[key]["min"]
        max_val = obj[key]["max"]
        random_val = random.randint(min_val, max_val)
        # vérifier si le nombre généré est dans l'intervalle
        if random_val < min_val:
            random_val = min_val
        elif random_val > max_val:
            random_val = max_val
        new_obj[key] = random_val
    total_sum = sum(new_obj.values())
    factor = n / total_sum
    for key in new_obj:
        new_obj[key] = int(new_obj[key] * factor)
    sum_new_obj = sum(new_obj.values())
    if sum_new_obj != n:
        diff = n - sum_new_obj
        # ajuster une valeur pour correspondre à la différence
        key_to_adjust = list(new_obj.keys())[0]
        new_obj[key_to_adjust] += diff
    return new_obj

# def generer_valeurs_aleatoires(obj_,nbr_questions):
#         """
#         Specifications :
#         Des intervales pour chaque thematique adpate les thématiques en fonction des min
#         et max dans le quels si possible la l'intervale est respectée mais la sommes 
#         de chaque valeur est égales au nombres de questions 
#         """
#         # result object_ du meme type de que celui en entrée 
#         result_obj_ = {}
#         for thematique_ in obj_:
#             # genere un nombre par rapport a l'intervale 
#             random_val_T = random.randint(obj_[thematique_]['min'], obj_[thematique_]['max'])
#             # Ajout dans l'object final
#             result_obj_[thematique_] = random_val_T
#         # Additionner tout les random de l'object
#         sum_total_T = sum(result_obj_.values())
#         factor_mult = (nbr_questions / sum_total_T)
#         for thematique_ in result_obj_:
#             result_obj_[thematique_] = int(result_obj_[thematique_] * factor_mult)
#         sum_total_T = sum(result_obj_.values())
#         if sum_total_T != nbr_questions:
#             # Thématique a ajuster pour avoir exactement nbr_questions apres avoir random et adapeter les nombres 
#             thematique_ajust = list(result_obj_.keys())[0]
#             result_obj_[thematique_ajust] += nbr_questions - sum_total_T
#         return result_obj_

import random

def generer_valeurs_aleatoires(obj_, nbr_questions):
    """
    Génère un objet de valeurs aléatoires pour chaque thématique,
    en respectant les intervalles min/max donnés dans l'objet en entrée.
    La somme des valeurs de chaque thématique est égale à nbr_questions.
    """
    # Création de l'objet résultat vide
    result_obj_ = {}
    for thematique_ in obj_:
        result_obj_[thematique_] = 0

    # Génération de valeurs aléatoires pour chaque thématique
    for thematique_ in obj_:
        # Intervalles min/max de la thématique
        min_val = obj_[thematique_]['min']
        max_val = obj_[thematique_]['max']
        # Génération d'un nombre aléatoire dans l'intervalle
        random_val_T = random.randint(min_val, max_val)
        # Ajout du nombre dans l'objet final
        result_obj_[thematique_] += random_val_T

    # Somme totale des valeurs générées
    sum_total_T = sum(result_obj_.values())

    # Si la somme totale est inférieure à nbr_questions, on ajuste une thématique
    if sum_total_T < nbr_questions:
        # Liste des thématiques triées par valeur décroissante
        thematiques_triees = sorted(result_obj_, key=result_obj_.get, reverse=True)
        for thematique_ajust in thematiques_triees:
            # Différence entre la valeur actuelle et la valeur maximale
            diff = obj_[thematique_ajust]['max'] - result_obj_[thematique_ajust]
            if diff > 0:
                # On peut ajouter au moins une valeur à cette thématique
                result_obj_[thematique_ajust] += min(diff, nbr_questions - sum_total_T)
                sum_total_T += diff
                # Si on a atteint la somme maximale, on sort de la boucle
                if sum_total_T == nbr_questions:
                    break

    # Si la somme totale est supérieure à nbr_questions, on ajuste une thématique
    elif sum_total_T > nbr_questions:
        # Liste des thématiques triées par valeur croissante
        thematiques_triees = sorted(result_obj_, key=result_obj_.get)
        for thematique_ajust in thematiques_triees:
            # Différence entre la valeur actuelle et la valeur minimale
            diff = result_obj_[thematique_ajust] - obj_[thematique_ajust]['min']
            if diff > 0:
                # On peut retirer au moins une valeur de cette thématique
                result_obj_[thematique_ajust] -= min(diff, sum_total_T - nbr_questions)
                sum_total_T -= diff
                # Si on a atteint la somme minimale, on sort de la boucle
                if sum_total_T == nbr_questions:
                    break

    return result_obj_


obj_ = {
    "math": {
        "min": 2,
        "max": 5
    },
    "histoire": {
        "min": 4,
        "max": 6
    },
    "informatique": {
        "min": 2,
        "max": 8,
    }
}

print(generer_valeurs_aleatoires(obj_, 15))
