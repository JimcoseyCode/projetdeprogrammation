import json
import secrets
from collections import Counter


# ? FONCTIONS UTILE :

def add_object_to_json(obj, type_objet, json_file):
    with open(json_file, "r") as file_:
        data = json.load(file_)
    # générer une clé unique pour l'objet
    key = type_objet + str(len(data) + 1)
    # ajouter l'objet à la liste existante avec la clé unique comme clé
    data[key] = obj
    data[key]["indice_question"] = str(len(data))
    # écrire les données modifiées dans le fichier JSON
    with open(json_file, "w") as file_:
        json.dump(data, file_, indent=4)


def add_object_to_json_student(obj, type_objet, json_file):
    with open(json_file, "r") as file_:
        data = json.load(file_)
    # générer une clé unique pour l'objet
    key = type_objet + str(len(data) + 1)
    # ajouter l'objet à la liste existante avec la clé unique comme clé
    data[key] = obj
    # écrire les données modifiées dans le fichier JSON
    with open(json_file, "w") as file_:
        json.dump(data, file_, indent=4)


def modif_question_to_json(obj, type_objet, indice_, json_file):
    with open(json_file, "r") as file_:
        data = json.load(file_)
    # générer une clé unique pour l'objet
    key = type_objet + secrets.token_hex(2)

    # ajouter l'objet à la liste existante avec la clé unique comme clé
    data[key] = obj
    data[key]["indice_question"] = str(indice_)

    # écrire les données modifiées dans le fichier JSON
    with open(json_file, "w") as file_:
        json.dump(data, file_, indent=4)


def compare_2_array(array1, array2):
    result_bool = True
    if len(array1) != len(array2):
        return False
    else:
        for i in range(len(array1)):
            if array1[i] != array2[i]:
                return False
    return result_bool


def arrays_have_common_elements(array1, array2):
    # Convertir les deux tableaux en ensemble pour utiliser l'intersection
    set1 = set(array1)
    set2 = set(array2)
    # Utiliser l'intersection pour savoir s'il y a des éléments en commun
    common_elements = set1.intersection(set2)
    # Si l'intersection est vide, il n'y a pas d'éléments en commun
    if len(common_elements) == 0:
        return False
    # Sinon, il y a des éléments en commun
    else:
        return True


def _array_equal_elements(array1, array2):
    return Counter(array1) == Counter(array2)


def student_exist(numero_etudiant):
    with open(f"components/user_info/students_DataBase.json", "r") as file_:
        buffer_read = json.load(file_)
        for key in buffer_read:
            if numero_etudiant == buffer_read[key]["numero_etudiant"]:
                return True
        return False


# ? ##################################################################
